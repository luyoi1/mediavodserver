//
// Created by luyoi on 2023/10/23.
//

#ifndef MEDIASIMPLESERVER_CONST_H
#define MEDIASIMPLESERVER_CONST_H

#endif //MEDIASIMPLESERVER_CONST_H

#define FMT_HEADER_ONLY

#include<iostream>
#include<spdlog/spdlog.h>
#include <filesystem>

#pragma once

namespace MS{
    inline constexpr int RTMP_VERSION=3;
}

inline constexpr unsigned short Recv_Udp_Buffer_Len=1500;
inline constexpr unsigned short Recv_Buffer_Len=1000;

inline constexpr unsigned short UdpListenPort=8080;

inline constexpr const char* SERVER_VERSION="luyoi1";



enum LoadType{
    H264=96,
    AAC=97,
};
enum ConnType{
    None,
    OverUdp,
    OverTcp
};
enum RtspMethod{
    OPTIONS,DESCRIBE,
    SETUP, PLAY,
    TEARDOWN,NONE,
};
enum SendType{
    RTP,
    RTCP,
    RTSP
};

struct UdpNode{
    UdpNode(SendType type,std::string ip,unsigned short port,unsigned int dataSize);

    SendType Type;
    std::string IP;
    unsigned short Port;
    std::shared_ptr<unsigned char> data;
    unsigned int dataLength;
};

struct TcpNode{
    TcpNode(SendType type,const std::shared_ptr<unsigned char>& dataPtr):
        Type(type),data(dataPtr)
    {

    }

    SendType Type;
    std::shared_ptr<unsigned char> data;
    unsigned char* dataPoint;
    size_t dataLength;//数据长度
};