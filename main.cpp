#include "Net/HTTP/Http_Sever.h"
#include "Net/RTMP/Rtmp_Server.h"
#include "Tool/IOContextPool.h"
#include "Tool/H264/H264Sink.h"
#include "Tool/H264/H264MediaSource.h"
#include "Tool/AAC/AACMediaSource.h"
#include "Tool/AAC/AACSink.h"
#include "Net/RTSP/Rtsp_Server.h"


int main() {
    try{
        //RTSP
        std::shared_ptr<Rtsp_Server> RtspServer=std::make_shared<Rtsp_Server>("0.0.0.0",8554,9090);
        auto session=std::make_shared<Session>("test");
        session->AddGop(0,H264Sink::Create(H264MediaSource::Create(IOContextPool::GetInstance()->GetContext(),"../data/daliu.h264")));
        session->AddGop(1,AACSink::Create(AACMediaSource::Create(IOContextPool::GetInstance()->GetContext(),"../data/daliu.aac")));
        RtspServer->AddSession(session->GetSessionName(),session);
        RtspServer->Start();

        //RTMP
        auto RtmpServer=std::make_shared<MS::Rtmp_Server>(IOContextPool::GetInstance()->GetContext(),"0.0.0.0",1935);
        RtmpServer->Start();

        //http-Server 支持点播,使用http-flv,hls协议
        std::shared_ptr<Http_Sever> Server=std::make_shared<Http_Sever>(IOContextPool::GetInstance()->GetContext(),"0.0.0.0",8080);
        Server->Start();


        boost::asio::signal_set signals(IOContextPool::GetInstance()->GetContext(), SIGINT, SIGTERM);
        signals.async_wait([&](auto, auto) {
            try {
                IOContextPool::GetInstance()->Stop();
            }
            catch (std::exception e) {
                spdlog::critical("error:{}", e.what());
                exit(0);
            }
            
        });

        while(IOContextPool::GetInstance()->GetRunSize()){}
    }catch(std::exception& ec){
        spdlog::critical("critical!message is {}",ec.what());
        exit(0);
    }
    return 0;
}
