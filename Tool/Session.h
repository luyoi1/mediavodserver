//
// Created by luyoi on 2023/10/21.
//

#ifndef MEDIASIMPLESERVER_SESSION_H
#define MEDIASIMPLESERVER_SESSION_H

#include<map>
#include "MediaGop.h"



class Session {
public:
    Session(std::string_view Name);
    ~Session();

    std::string_view GetSessionName();

    void AddGop(int16_t MediaID, std::shared_ptr<Sink> sink);
    void RemoveGop(int16_t MediaID);
    std::shared_ptr<MediaGopInfo> GetGop(int16_t MediaID);
    const std::map<int16_t, std::shared_ptr<MediaGopInfo>>& GetGopMap();
    void AddConn(int32_t id);
    void RemoveConn(int32_t id);
private:
    std::string SessionName;

    std::mutex GopMapMtx;
    std::map<int16_t, std::shared_ptr<MediaGopInfo>> GopMap;
    std::mutex ConnMapMtx;
    std::map<int32_t,bool> ConnMap;
};


#endif //MEDIASIMPLESERVER_SESSION_H
