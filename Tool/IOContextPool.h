//
// Created by luyoi on 2023/10/18.
//

#ifndef MEDIASIMPLESERVER_IOCONTEXTPOOL_H
#define MEDIASIMPLESERVER_IOCONTEXTPOOL_H



#include<boost/asio.hpp>
#include<thread>
#include<vector>
#include"../constant/Const.h"

class IOContextPool {
public:
    static IOContextPool* GetInstance();
    boost::asio::io_context& GetContext();
    void Stop();
    size_t GetRunSize();
private:
    IOContextPool(size_t threadSize=1);
    ~IOContextPool();

    std::mutex mtx;
    size_t ContextNum;
    std::atomic_int RunSize;
    int randNum;
    std::vector<std::thread> ThreadVector;
    std::vector<std::shared_ptr<boost::asio::io_context>> ContextVector;
    std::vector<std::shared_ptr<boost::asio::io_context::work>> WorkVector;
};


#endif //MEDIASIMPLESERVER_IOCONTEXTPOOL_H
