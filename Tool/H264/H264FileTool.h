//
// Created by luyoi on 2023/10/19.
//

#ifndef MEDIASIMPLESERVER_H264FILETOOL_H
#define MEDIASIMPLESERVER_H264FILETOOL_H

#endif //MEDIASIMPLESERVER_H264FILETOOL_H

#include<iostream>

static inline int startCode3(uint8_t* buf)
{
    if (buf[0] == 0 && buf[1] == 0 && buf[2] == 1)
        return 1;
    else
        return 0;
}

static inline int startCode4(uint8_t* buf)
{
    if (buf[0] == 0 && buf[1] == 0 && buf[2] == 0 && buf[3] == 1)
        return 1;
    else
        return 0;
}

static uint8_t* findNextStartCode(uint8_t* buf, int len)
{
    int i;

    if (len < 3)
        return nullptr;

    for (i = 0; i < len - 3; ++i)
    {
        if (startCode3(buf) || startCode4(buf))
            return buf;

        ++buf;
    }

    if (startCode3(buf))
        return buf;

    return nullptr;
}