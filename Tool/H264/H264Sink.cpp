//
// Created by luyoi on 2023/10/20.
//

#include "H264Sink.h"

H264Sink::H264Sink(const std::shared_ptr<MediaSource>& mediaSource):
        Sink(mediaSource,LoadType::H264),ClockRate(90000)
        ,Fps(mediaSource->getFps())
{

}

H264Sink::~H264Sink() {

}

std::string H264Sink::GetMediaDescription(unsigned short port) {
    auto str=fmt::format("m=video {} RTP/AVP {}",port,PayloadType);
    return str;
}

std::string H264Sink::GetAttribute() {
    auto str=fmt::format("a=rtpmap:{} H264/{}\r\n",PayloadType,ClockRate);
    str+=fmt::format("a=framerate:{}",Fps);
    return str;
}

uint16_t H264Sink::InitFrame(std::shared_ptr<MediaFrame> frame, SendRtpPack func, uint32_t *TimeStamp) {
    if(isStop){
        return 0;
    }
    auto seqSize=0;
    // 发送RTP数据包
    uint8_t naluType = frame->dataPoint[0];
    if (frame->dataLength <= RTP_MAX_PKT_SIZE){
        //auto nowTime=std::chrono::system_clock::now().time_since_epoch();
        auto rtpPack=std::make_shared<RtpPacket>(frame->dataLength);
        memcpy(rtpPack->RtpHead->payload,frame->dataPoint,frame->dataLength);

        func(rtpPack);

        seqSize++;
        //spdlog::info("{}",std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()-nowTime).count());
        if ((naluType & 0x1F) == 6 ||(naluType & 0x1F) == 7 || (naluType & 0x1F) == 8) // 如果是SPS、PPS就不需要加时间戳
        {
            //
            // SinkMediaSoucre->putFrameToInputQueue(frame);
            return seqSize;
        }
    }
    else{
        int pktNum = frame->dataLength / RTP_MAX_PKT_SIZE;       // 有几个完整的包
        int remainPktSize = frame->dataLength % RTP_MAX_PKT_SIZE; // 剩余不完整包的大小
        int i, pos = 1;

        /* 发送完整的包 */
        for (i = 0; i < pktNum; i++)
        {
            auto rtpPack=std::make_shared<RtpPacket>(2+ RTP_MAX_PKT_SIZE);
            memcpy(rtpPack->RtpHead->payload+2,frame->dataPoint+pos,RTP_MAX_PKT_SIZE);
            rtpPack->RtpHead->payload[0] = (naluType & 0x60) | 28; //(naluType & 0x60)表示nalu的重要性，28表示为分片
            rtpPack->RtpHead->payload[1] = naluType & 0x1F;

            if (i == 0) //第一包数据
                rtpPack->RtpHead->payload[1] |= 0x80; // start
            else if (remainPktSize == 0 && i == pktNum - 1) //最后一包数据
                rtpPack->RtpHead->payload[1] |= 0x40; // end

            func(rtpPack);
            seqSize++;
            pos += RTP_MAX_PKT_SIZE;
        }

        /* 发送剩余的数据 */
        if (remainPktSize > 0)
        {
            auto rtpPack=std::make_shared<RtpPacket>(2+ remainPktSize);
            memcpy(rtpPack->RtpHead->payload+2,frame->dataPoint+pos,remainPktSize);
            rtpPack->RtpHead->payload[0] = (naluType & 0x60) | 28;
            rtpPack->RtpHead->payload[1] = naluType & 0x1F;
            rtpPack->RtpHead->payload[1] |= 0x40; //end

            func(rtpPack);
            seqSize++;
        }
    }
    *TimeStamp += ClockRate / Fps;
    //使用完的帧包放回输入队列
    //SinkMediaSoucre->putFrameToInputQueue(frame);
    return seqSize;
}

/*
void H264Sink::SendNode(std::shared_ptr<RtpPacket> &node) {
    SendPack(node,RtpChannel,RemoteLocalRtpPort);
}*/

std::shared_ptr<Sink> H264Sink::Create(std::shared_ptr<MediaSource> mediaSource) {
    return std::make_shared<H264Sink>(mediaSource);
}

uint32_t H264Sink::CheckTime(char *dataPtr) {
    uint8_t naluType=dataPtr[0]&0x1F;
    if(naluType==6||naluType==7||naluType==8){
        return 0;
    }
    return ClockRate / Fps;
}

void H264Sink::CheckIndex(char *dataPtr, const uint32_t nowSize, uint32_t *PreIndex,std::map<uint32_t, uint32_t> &MetaTimeMap) {
    return;
}


