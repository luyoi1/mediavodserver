//
// Created by luyoi on 2023/10/19.
//

#ifndef MEDIASIMPLESERVER_H264MEDIASOURCE_H
#define MEDIASIMPLESERVER_H264MEDIASOURCE_H

#include "../Rtsp/Rtp.h"
#include "../Base/MediaSource.h"
#include <iostream>
#include <fstream>

class H264MediaSource: public MediaSource{
public:
    static std::shared_ptr<MediaSource> Create(boost::asio::io_context& ioc,std::filesystem::path FilePath);
    H264MediaSource(boost::asio::io_context& ioc,const std::filesystem::path& FileName);
    virtual ~H264MediaSource();
    virtual void Start();
private:
    virtual void Handler(std::shared_ptr<MediaSource> node);
    int GetFrameFromH264File(const std::shared_ptr<unsigned char>& FrameBuffer,int BufferSize);

    std::shared_ptr<std::fstream> FileStream;
};


#endif //MEDIASIMPLESERVER_H264MEDIASOURCE_H
