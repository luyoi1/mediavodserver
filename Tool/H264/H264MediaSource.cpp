//
// Created by luyoi on 2023/10/19.
//

#include <filesystem>
#include "H264MediaSource.h"
#include "H264FileTool.h"

H264MediaSource::H264MediaSource(boost::asio::io_context& ioc,const std::filesystem::path& FileName):
        MediaSource(ioc,FileName.string()),FileStream(std::make_shared<std::fstream>(FileName,std::ios::in|std::ios::binary))
{
    SourceName=FileName.filename().string();
    if(!FileStream->is_open()){
        spdlog::error("H264 MediaSource Init Error!{} Not Find",SourceName);
        return;
    }
    setFps(25);

}

H264MediaSource::~H264MediaSource() {
    if(FileStream->is_open()){
        FileStream->close();
    }

}

void H264MediaSource::Handler(std::shared_ptr<MediaSource> node) {//处理输入队列里的空帧
    if(isStop){
        return;
    }
    std::shared_ptr<MediaFrame> frame;
    {
        std::unique_lock<std::mutex> uql(this->MediaInputMux);//加锁
        if(FrameInputQueue.empty()){
            spdlog::warn("H264 FrameInputQueue is Empty!But is Post Task!");
            return;
        }
        frame=FrameInputQueue.front();
        FrameInputQueue.pop();
    }


    int StartCodeNum = 0;

    while (true){
        frame->dataLength= GetFrameFromH264File(frame->buffer,FRAME_MAX_SIZE);
        if(frame->dataLength<0){
            spdlog::error("GetFrameFromH264File Read Error!");
            this->putFrameToInputQueue(frame);
            return;
        }
        if(startCode3(frame->buffer.get())){
            StartCodeNum=3;
        }else{
            StartCodeNum=4;
        }
        frame->dataPoint=frame->buffer.get()+StartCodeNum;//指向数据
        frame->dataLength-=StartCodeNum;

        unsigned char naluType = frame->dataPoint[0]&0x1F;

        if(0x09==naluType){//需要丢弃的包
            continue;
        }
        //其余的包
        break;
    }
    std::unique_lock<std::mutex> uql(this->MediaOutputMux);//加锁
    FrameOutputQueue.push(frame);
    if(FrameOutputQueue.size()==1){
        FrameCond.notify_one();
    }
}

int H264MediaSource::GetFrameFromH264File(const std::shared_ptr<unsigned char>&FrameBuffer, int BufferSize) {
    if(FrameBuffer== nullptr){
        return -1;
    }

    int ReadSize=0,FrameSize=0;
    unsigned char* nextStartCode;
    ReadSize=FileStream->read((char*)FrameBuffer.get(),BufferSize).gcount();
    if(!startCode3(FrameBuffer.get())&&!startCode4(FrameBuffer.get())){
        memset(FrameBuffer.get(),0,BufferSize);//跳过该帧
        spdlog::error("Read {} Error,Is Not Have Startcode!",this->getSourceName().data());
        return -1;
    }

    nextStartCode= findNextStartCode(FrameBuffer.get()+3,ReadSize-3);
    if(!nextStartCode){
        FrameSize=ReadSize;
        FileStream->seekp(0,std::ios::beg);
        spdlog::error("Read {} Error,Is Not Have Next Startcode!",this->getSourceName().data());
        return FrameSize;
    }
    FrameSize=(nextStartCode-FrameBuffer.get());
    FileStream->seekp(FrameSize-ReadSize,std::ios::cur);//移动指针到帧尾位置
    return FrameSize;
}

std::shared_ptr<MediaSource>  H264MediaSource::Create(boost::asio::io_context &ioc,std::filesystem::path FilePath) {
    return std::make_shared<H264MediaSource>(ioc, FilePath);
}

void H264MediaSource::Start() {
    for(size_t i=0;i<FRAME_BUFFER_LENGTH;i++){
        this->putFrameToInputQueue(std::make_shared<MediaFrame>());
    }
}
