//
// Created by luyoi on 2023/10/20.
//

#ifndef MEDIASIMPLESERVER_H264SINK_H
#define MEDIASIMPLESERVER_H264SINK_H

#include<map>
#include"../base/Sink.h"


class H264Sink: public Sink{
public:
    static std::shared_ptr<Sink> Create(std::shared_ptr<MediaSource> mediaSource);
    H264Sink(const std::shared_ptr<MediaSource>& mediaSource);
    virtual ~H264Sink();
    virtual std::string GetMediaDescription(unsigned short port);
    virtual std::string GetAttribute();

    virtual uint32_t CheckTime(char *dataPtr);

    virtual void CheckIndex(char *dataPtr, const uint32_t nowSize, uint32_t *PreIndex,std::map<uint32_t, uint32_t> &MetaTimeMap);

protected:
    //virtual void InitFrame(std::shared_ptr<MediaFrame> frame,SendRtpPack func);
    virtual uint16_t InitFrame(std::shared_ptr<MediaFrame> frame, SendRtpPack func, uint32_t *TimeStamp);
    //virtual void SendNode(std::shared_ptr<RtpPacket>& node);
private:
    int ClockRate;
    int Fps;
};


#endif //MEDIASIMPLESERVER_H264SINK_H
