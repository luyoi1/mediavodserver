//
// Created by luyoi on 2023/10/18.
//

#include "Rtp.h"

RtpPacket::RtpPacket():
    Buffer(new unsigned char[4+RTP_HEADER_SIZE+RTP_MAX_PKT_SIZE+100]),
    OverTcpHeaderPointer(Buffer.get()),OverUdpHeaderPointer(Buffer.get()+4)
    ,RtpHead((RtpHeader*)OverUdpHeaderPointer),dataLength(0)
{

}

RtpPacket::~RtpPacket() {
    OverUdpHeaderPointer= nullptr;
    RtpHead = nullptr;
}

void RtpPacket::ResetBuffer(std::shared_ptr<unsigned char> &BufferNode,size_t length) {
    Buffer=BufferNode;
    OverTcpHeaderPointer=Buffer.get();
    OverUdpHeaderPointer=Buffer.get()+4;
    RtpHead=(RtpHeader*)OverUdpHeaderPointer;
    dataLength = length;
}

RtpPacket::RtpPacket(size_t dataSize):
        Buffer(new unsigned char[4+RTP_HEADER_SIZE+dataSize]),OverTcpHeaderPointer(Buffer.get()),
        OverUdpHeaderPointer(Buffer.get()+4),RtpHead((RtpHeader*)OverUdpHeaderPointer),
        dataLength(4+RTP_HEADER_SIZE+dataSize)
{

}

void parseRtpHeader(unsigned char* buf, struct RtpHeader* rtpHeader){
    memset(rtpHeader,0,sizeof(RtpHeader));

    // byte 0
    rtpHeader->version = (buf[0] & 0xC0) >> 6;
    rtpHeader->padding = (buf[0] & 0x20) >> 5;
    rtpHeader->extension = (buf[0] & 0x10) >> 4;
    rtpHeader->csrcLen = (buf[0] & 0x0F);
    // byte 1
    rtpHeader->marker = (buf[1] & 0x80) >> 7;
    rtpHeader->payloadType = (buf[1] & 0x7F);
    // bytes 2,3
    rtpHeader->seq = ((buf[2] & 0xFF) << 8) | (buf[3] & 0xFF);
    // bytes 4-7
    rtpHeader->timestamp = ((buf[4] & 0xFF) << 24) | ((buf[5] & 0xFF) << 16)
                           | ((buf[6] & 0xFF) << 8)
                           | ((buf[7] & 0xFF));
    // bytes 8-11
    rtpHeader->ssrc = ((buf[8] & 0xFF) << 24) | ((buf[9] & 0xFF) << 16)
                      | ((buf[10] & 0xFF) << 8)
                      | ((buf[11] & 0xFF));
}
void parseRtcpHeader(unsigned char* buf, struct RtcpHeader* rtcpHeader){
    memset(rtcpHeader, 0, sizeof(RtcpHeader));
    // byte 0
    rtcpHeader->version = (buf[0] & 0xC0) >> 6;
    rtcpHeader->padding = (buf[0] & 0x20) >> 5;
    rtcpHeader->rc = (buf[0] & 0x1F);
    // byte 1
    rtcpHeader->packetType = (buf[1] & 0xFF);
    // bytes 2,3
    rtcpHeader->length= ((buf[2] & 0xFF) << 8) | (buf[3] & 0xFF);
}