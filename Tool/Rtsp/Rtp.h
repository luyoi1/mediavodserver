//
// Created by luyoi on 2023/10/18.
//

#ifndef MEDIASIMPLESERVER_RTP_H
#define MEDIASIMPLESERVER_RTP_H

#include<iostream>

constexpr unsigned short RTP_VESION=2;
constexpr unsigned short RTP_PAYLOAD_TYPE_H264=96;
constexpr unsigned short RTP_PAYLOAD_TYPE_AAC=97;
constexpr unsigned short RTP_HEADER_SIZE=12;
constexpr unsigned short RTP_MAX_PKT_SIZE=1400;


struct RtpHeader {
    // byte 0
    unsigned char csrcLen:4;// contributing source identifiers count
    unsigned char extension:1;
    unsigned char padding:1;
    unsigned char version:2;

    // byte 1
    unsigned char payloadType:7;
    unsigned char marker:1;

    // bytes 2,3
    unsigned short seq;

    // bytes 4-7
    unsigned int timestamp;

    // bytes 8-11
    unsigned int ssrc;

    // data
    unsigned char payload[0];
};

struct RtcpHeader{
    //byte1
    unsigned char rc:5;
    unsigned char padding:1;
    unsigned char version:2;
    //byte 2
    unsigned char packetType;
    //byte 3-4
    unsigned short length;
};

class RtpPacket{
public:
    RtpPacket();
    RtpPacket(size_t dataSize);
    ~RtpPacket();

    void ResetBuffer(std::shared_ptr<unsigned char>& Buffer,size_t length);

    std::shared_ptr<unsigned char> Buffer;
    unsigned char* OverTcpHeaderPointer;
    unsigned char* OverUdpHeaderPointer;
    RtpHeader *RtpHead;
    int dataLength;
};

void parseRtpHeader(unsigned char* buf, struct RtpHeader* rtpHeader);
void parseRtcpHeader(unsigned char* buf, struct RtcpHeader* rtcpHeader);

#endif //MEDIASIMPLESERVER_RTP_H
