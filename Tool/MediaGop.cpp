//
// Created by luyoi on 2023/11/19.
//

#include "MediaGop.h"

MediaGopInfo::MediaGopInfo(std::shared_ptr<Sink>& sink)
        :MediaSinkInfo(sink),GopMemSize(0),GopWaitSize(0)
{
    MediaSinkInfo->SetSaveFunc(std::bind(&MediaGopInfo::SaveGop,this,std::placeholders::_1));
}

void MediaGopInfo::SaveGop(std::shared_ptr<MediaFrame> mediaframe)
{
    std::lock_guard<std::mutex> lk(GopMtx);
    std::shared_ptr<MediaFrame> node=std::make_shared<MediaFrame>(mediaframe->dataLength);
    node->dataPoint=node->buffer.get();
    memcpy(node->dataPoint, mediaframe->dataPoint, mediaframe->dataLength);
    GopMem.emplace_back(node);
    GopMemSize.store(GopMem.size());
    TimeStampOffest +=MediaSinkInfo->CheckTime((char *) node->dataPoint);
    MediaSinkInfo->CheckIndex((char*)node->dataPoint,GopMemSize, &preMetaIndex, GopMetaTimeMap);
    GopCond.notify_all();
}

std::shared_ptr<MediaFrame> MediaGopInfo::GetGop(int32_t GopIndex)
{
    if(GopIndex<0)
    {
        return {nullptr};
    }

    if((GopIndex+1)>GopMemSize.load())
    {

        std::unique_lock<std::mutex> lk(GopMtx);
        GopWaitSize.store(GopWaitSize.load()+1);
        /*
        if (GopWaitSize.load() == 1)
        {
            StartReadMedia();
        }
        */
        GopCond.wait(lk, [this,GopIndex]{
            return GopMemSize.load() >= GopIndex+1;
        });
        GopWaitSize.store(GopWaitSize.load()-1);
        /*
        if (GopWaitSize.load() == 0)
        {
            StopReadMedia();
        }
        */
        if (GopMemSize==0)
            return {nullptr};
    }

    return GopMem[GopIndex];

}

int32_t MediaGopInfo::GetStartGopIndex(uint32_t TimeStampOffest)
{

    auto iter=GopMetaTimeMap.lower_bound(TimeStampOffest);
    if(iter==GopMetaTimeMap.end())
    {
        return -1;
    }
    return iter->second;
}

void MediaGopInfo::StartReadMedia()const
{
    MediaSinkInfo->RunTimer();
}

void MediaGopInfo::StopReadMedia()const
{
    MediaSinkInfo->StopTimer();
}

void MediaGopInfo::ClearGop()
{
    std::lock_guard<std::mutex> lk(GopMtx);
    GopMemSize.store(0);
    GopMem.clear();
    GopMetaTimeMap.clear();
    while(GopWaitSize.load())
    {
        GopCond.notify_one();
    }
    preMetaIndex = 0;
    //MediaSinkInfo->Clear();
}

std::shared_ptr<Sink> MediaGopInfo::GetSink() {
    return MediaSinkInfo;
}


