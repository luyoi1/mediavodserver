//
// Created by luyoi on 2023/10/18.
//

#include "IOContextPool.h"

IOContextPool *IOContextPool::GetInstance() {
    static IOContextPool instance(std::thread::hardware_concurrency());
    return &instance;
}

boost::asio::io_context &IOContextPool::GetContext() {
    std::lock_guard<std::mutex> lk(mtx);
    auto point=randNum;
    randNum++;
    randNum%=ContextNum;//rand-robin
    return *ContextVector[point];
}

IOContextPool::IOContextPool(size_t threadSize):
    ContextNum(threadSize),randNum(0)
{
    ContextVector.resize(ContextNum);
    WorkVector.resize(ContextNum);
    ThreadVector.resize(ContextNum);
    for (size_t i = 0; i < ContextNum; ++i) {
        ContextVector[i]=std::move(std::make_unique<boost::asio::io_context>(1));
        WorkVector[i]=std::move(std::make_unique<boost::asio::io_context::work>(*this->ContextVector[i]));
        ThreadVector[i]=std::move(std::thread([this,i]{
            this->ContextVector[i]->run();
        }));
    }
    RunSize.store(ContextNum);
}

IOContextPool::~IOContextPool() {
    if(!WorkVector.empty()){
        Stop();
    }
}

void IOContextPool::Stop() {
    spdlog::info("server stop!");
    WorkVector.clear();
    for(auto& ioc:ContextVector){
        if (!ioc->stopped()) {
            ioc->stop();
        }
        
    }
    for(auto& t:ThreadVector){
        if(t.joinable()){
            t.detach();
        }
    }
    RunSize.store(0);
}

size_t IOContextPool::GetRunSize() {
    return RunSize.load();
}
