//
// Created by luyoi on 2023/11/13.
//

#ifndef MEDIASIMPLESERVER_RTMPCHUNK_H
#define MEDIASIMPLESERVER_RTMPCHUNK_H

#pragma once

#include <iostream>
#include <map>
#include <vector>
#include "RtmpMessage.h"
#include "../Amf/AmfTool.h"

namespace MS {



    class RtmpChunk {
    public:
        enum class State{
            PARSE_HEADER,
            PARSE_BODY
        };
        RtmpChunk();
        void SetInChunkSize(unsigned int size);
        void SetOutChunkSize(unsigned int size);
        int GetStreamID();
        int Parse(char* Buf,size_t BufLen,RtmpMessage& OutMsg);
        int CreateChunk(unsigned int ChunkStreamID,RtmpMessage& msg,char* Buf,unsigned int BufLen);
        void Clear();
    private:
        int ParseChunkHeader(char* Buf,size_t BufLen);
        int ParseChunkBody(char* Buf,size_t BufLen);
        int CreateBasicHeader(uint8_t Format,unsigned int ChunkStreamID,char* Buf);
        int CreateMessageHeader(ChunkFormat Format,RtmpMessage& msg,char* Buf);

        State ChunkState;
        int PreChunkStraemID;
        int StreamID;
        unsigned int InChunkSize=RTMP_DEFAULT_CHUNKSIZE;
        unsigned int OutChunkSize=RTMP_DEFAULT_CHUNKSIZE;

        std::map<int,RtmpMessage> RtmpMessageMap;

        std::vector<int> ChunkHeadLens;
    };

} // MS

#endif //MEDIASIMPLESERVER_RTMPCHUNK_H
