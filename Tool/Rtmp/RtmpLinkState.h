//
// Created by luyoi on 2023/11/14.
//

#ifndef MEDIASIMPLESERVER_RTMPLINKSTATE_H
#define MEDIASIMPLESERVER_RTMPLINKSTATE_H

#include <iostream>
#include <librtmp/rtmp.h>

namespace MS {
    constexpr int RTMP_CHUNK_CONTROL_ID  = 2;
    constexpr int RTMP_CHUNK_INVOKE_ID   = 3;
    constexpr int RTMP_CHUNK_AUDIO_ID    = 4;
    constexpr int RTMP_CHUNK_VIDEO_ID    = 5;
    constexpr int RTMP_CHUNK_DATA_ID     = 6;

    constexpr int RTMP_CODEC_ID_H264     = 7;
    constexpr int RTMP_CODEC_ID_AAC      = 10;
    constexpr int RTMP_CODEC_ID_G711A    = 7;
    constexpr int RTMP_CODEC_ID_G711U    = 8;

    constexpr int RTMP_AVC_SEQUENCE_HEADER = 0x18;
    constexpr int RTMP_AAC_SEQUENCE_HEADER = 0x19;

    struct MediaInfo{
        unsigned char videoCodecID = RTMP_CODEC_ID_H264;
        unsigned char videoFrameRate = 0;
        unsigned int videoWidth = 0;
        unsigned int videoHeight = 0;
        std::shared_ptr<unsigned char> sps;
        std::shared_ptr<unsigned char> pps;
        std::shared_ptr<unsigned char> sei;
        unsigned int sps_size=0;
        unsigned int pps_size=0;
        unsigned int sei_size=0;

        unsigned char audioCodecID = RTMP_CODEC_ID_AAC;
        unsigned int audioChannel = 0;
        unsigned int audioSamplerate = 0;
        unsigned int audioFrameLen = 0;

        std::shared_ptr<unsigned char> audioSpecificConfig;
        unsigned int audioSpecificConfigSize = 0;

    };


    class RtmpLinkState {
    public:
        virtual ~RtmpLinkState()=default;

        void SetChunkSize(size_t size);
        void SetGopCacheLen(size_t Len=5000);
        void SetPeerBandWidth(size_t size);
        size_t getPeerBandWidth() const;
        size_t getAckNowLedgement() const;
        size_t getMaxChunkSize() const;
        size_t getMaxGopCacheLen() const;
        std::string_view getUrl() const;
        std::string_view getTcUrl() const;
        std::string_view getSwfUrl() const;
        std::string_view getApp() const;
        std::string_view getStreamName() const;
        std::string_view getStreamPath() const;

        unsigned short port = 1935;
        std::string Url;
        std::string TcUrl,SwfUrl;
        std::string Ip;
        std::string App;
        std::string StreamName;
        std::string StreamPath;

        size_t PeerBandWidth=5000000;
        size_t AckNowLedgement = 5000000;
        size_t MaxChunkSize = RTMP_DEFAULT_CHUNKSIZE;
        size_t MaxGopCacheLen= 0;
    };

} // MS

#endif //MEDIASIMPLESERVER_RTMPLINKSTATE_H
