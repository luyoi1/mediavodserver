//
// Created by luyoi on 2023/11/13.
//

#include "RtmpChunk.h"
#include "../Amf/AmfTool.h"


namespace MS {

    RtmpChunk::RtmpChunk():
    ChunkHeadLens({11,7,3,0})
    {
        ChunkState=State::PARSE_HEADER;
        PreChunkStraemID=-1;
        StreamID=1;
    }

    void RtmpChunk::SetInChunkSize(unsigned int size) {
        InChunkSize=size;
    }

    void RtmpChunk::SetOutChunkSize(unsigned int size) {
        OutChunkSize=size;
    }

    int RtmpChunk::GetStreamID() {
        return StreamID;
    }

    int RtmpChunk::Parse(char *Buf, size_t BufLen, RtmpMessage&OutMsg) {
        int byteUsed=0;
        if(BufLen==0){
            return 0;
        }

        if(ChunkState==State::PARSE_HEADER){
            byteUsed=ParseChunkHeader(Buf,BufLen);
        }else if(ChunkState==State::PARSE_BODY){
            byteUsed= ParseChunkBody(Buf,BufLen);
            if(byteUsed<0||PreChunkStraemID<0){
                return byteUsed;
            }
            auto& Msg=RtmpMessageMap[PreChunkStraemID];
            if(Msg.isReady()){
                if (Msg.TimeStamp>=0xFFFFFF) {
                    Msg.LongTimeStamp += Msg.extend_timestamp;
                }
                else {
                    Msg.LongTimeStamp += Msg.TimeStamp;
                }
                OutMsg=Msg;
                PreChunkStraemID=-1;
                Msg.Clear();//注意一些修改的局部变量
            }
        }
        return byteUsed;
    }

    int RtmpChunk::ParseChunkHeader(char*Buf, size_t BufLen) {
        uint32_t byteUsed=0;
        uint8_t* ptr=(uint8_t*)Buf;

        uint8_t byte=(ptr+byteUsed)[0];
        byteUsed+=1;
        uint8_t Format=byte>>6;
        if(Format>=4){
            return -1;
        }
        uint8_t chunkStreamID=byte&0x3F;
        if (chunkStreamID == 0) { // csid [64, 319]
            if (BufLen < (byteUsed + 2)) {
                return 0;
            }
            chunkStreamID += ptr[byteUsed] + 64;
            byteUsed += 1;
        }
        else if (chunkStreamID == 1) { // csid [64, 65599]
            if (BufLen < (3 + byteUsed)) {
                return 0;
            }
            chunkStreamID += ptr[byteUsed + 1] * 255 + ptr[byteUsed] + 64;
            byteUsed += 2;
        }

        uint32_t HeadLength=ChunkHeadLens[Format];
        if(BufLen<(HeadLength+byteUsed)){//收到的数据不足头部长度
            return 0;
        }

        RtmpMessageHeader header{};
        memset(&header,0,sizeof(RtmpMessageHeader));
        memcpy(&header,ptr+byteUsed,HeadLength);
        byteUsed+=HeadLength;

        auto&msg=RtmpMessageMap[chunkStreamID];
        msg.ChunkStreamID=PreChunkStraemID=chunkStreamID;
        if((ChunkFormat)HeadLength==ChunkFormat::CHUNK_TYPE_0||(ChunkFormat)HeadLength==ChunkFormat::CHUNK_TYPE_1) {
            uint32_t length=ReadUint24BE((char*)header.BodyLength);
            if(msg.BodyLength!=length||!msg.Payload) {
                msg.BodyLength=length;
                msg.Payload.reset(new char[msg.BodyLength]);
            }
            msg.BodyIndex=0;
            msg.TypeID=header.TypeID;
        }

        if((ChunkFormat)HeadLength==ChunkFormat::CHUNK_TYPE_0) {
            msg.StreamID=ReadUint24LE((char*)header.StreamID);
        }

        uint32_t timestamp=ReadUint24BE((char*)header.TimeStamp);
        uint32_t extendTime=0;
        if(timestamp>=0x00FFFFFF||msg.LongTimeFlag){
            if(BufLen<(4+byteUsed)){
                return 0;
            }
            extendTime=ReadUint32BE((char*)ptr+byteUsed);
            byteUsed+=4;
        }

        if(msg.BodyIndex==0) {
            if ((ChunkFormat)HeadLength == ChunkFormat::CHUNK_TYPE_0) {
                // absolute timestamp
                msg.LongTimeStamp = 0;
                msg.TimeStamp = timestamp;
                msg.extend_timestamp = extendTime;
            }
            else {
                // relative timestamp (timestamp delta)
                if (msg.TimeStamp>=0xFFFFFF) {
                    msg.extend_timestamp += extendTime;
                }
                else {
                    msg.TimeStamp += timestamp;
                }
            }
        }


        ChunkState=State::PARSE_BODY;
        return byteUsed;
    }

    int RtmpChunk::ParseChunkBody(char *Buf, size_t BufLen) {
        unsigned int byteUsed=0;
        unsigned char* ptr=(unsigned char*)Buf;

        if(PreChunkStraemID<0){
            return -1;
        }
        auto& msg=RtmpMessageMap[PreChunkStraemID];
        unsigned int ChunkRemainSize=msg.BodyLength-msg.BodyIndex;
        if(ChunkRemainSize>InChunkSize){
            ChunkRemainSize=InChunkSize;
        }
        if(BufLen<ChunkRemainSize){
            return 0;
        }
        if(msg.BodyIndex+ChunkRemainSize>msg.BodyLength){
            return -1;
        }

        memcpy(msg.Payload.get()+msg.BodyIndex,ptr+byteUsed,ChunkRemainSize);
        byteUsed+=ChunkRemainSize;
        msg.BodyIndex+=ChunkRemainSize;

        if(msg.isReady()||msg.BodyIndex%InChunkSize==0){
            ChunkState=State::PARSE_HEADER;
        }
        return byteUsed;
    }

    int RtmpChunk::CreateChunk(unsigned int ChunkStreamID, RtmpMessage &msg, char *Buf, unsigned int BufLen) {
        unsigned int buf_offset = 0, payload_offset = 0;
        unsigned int capacity=msg.BodyLength+msg.BodyLength/OutChunkSize*5;
        if(BufLen<capacity){
            return -1;
        }
        buf_offset += CreateBasicHeader(0, ChunkStreamID, Buf + buf_offset); //first chunk
        buf_offset += CreateMessageHeader(ChunkFormat::CHUNK_TYPE_0, msg, Buf + buf_offset);

        if(msg.LongTimeFlag){
            unsigned int timestamp=(unsigned int)msg.LongTimeStamp;
            timestamp= htonl(timestamp);
            memcpy(Buf+buf_offset,&timestamp,4);
            buf_offset+=4;
        }
        int Length=msg.BodyLength;
        while(Length>0){
            if (Length > OutChunkSize) {
                memcpy(Buf + buf_offset, msg.Payload.get() + payload_offset, OutChunkSize);
                payload_offset += OutChunkSize;
                buf_offset += OutChunkSize;
                Length -= OutChunkSize;

                buf_offset += CreateBasicHeader(3, ChunkStreamID, Buf + buf_offset);
                if (msg.LongTimeFlag) {
                    unsigned int timestamp=(unsigned int)msg.LongTimeStamp;
                    timestamp= htonl(timestamp);
                    memcpy(Buf+buf_offset,&timestamp,4);
                    buf_offset += 4;
                }
            }
            else {
                memcpy(Buf + buf_offset, msg.Payload.get() + payload_offset, Length);
                buf_offset += Length;
                Length = 0;
                break;
            }
        }
        return buf_offset;
    }

    void RtmpChunk::Clear() {
        RtmpMessageMap.clear();
    }

    int RtmpChunk::CreateBasicHeader(uint8_t Format, unsigned int ChunkStreamID, char *Buf) {
        int Len=0;
        if(ChunkStreamID>=64+255){
            Buf[Len++] = (Format << 6) | 1;
            Buf[Len++] = (ChunkStreamID - 64) & 0xFF;
            Buf[Len++] = ((ChunkStreamID - 64) >> 8) & 0xFF;
        }else if(ChunkStreamID>=64){
            Buf[Len++]=(Format << 6) | 0;
            Buf[Len++]=(ChunkStreamID-64)&0xFF;
        }else{
            Buf[Len++]=(Format << 6) | ChunkStreamID;
        }
        return Len;
    }

    int RtmpChunk::CreateMessageHeader(ChunkFormat Format, RtmpMessage &msg, char *Buf) {
        int Len=0;
        if(Format>=ChunkFormat::CHUNK_TYPE_2){
            unsigned int timestamp=0;
            if(msg.LongTimeFlag){
                WriteUint24BE((char*)Buf+Len,0xFFFFFF);
            }else{
                WriteUint24BE((char*)Buf+Len,msg.TimeStamp);
            }
            Len+=3;
        }
        if(Format>=ChunkFormat::CHUNK_TYPE_1){
            WriteUint24BE((char*)Buf+Len,msg.BodyLength);
            Len+=3;
            Buf[Len++]=msg.TypeID;
        }
        if(Format==ChunkFormat::CHUNK_TYPE_0){
            memcpy(Buf+Len,&msg.StreamID,4);
            Len+=4;
        }
        return Len;
    }

} // MS