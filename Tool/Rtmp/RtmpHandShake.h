//
// Created by luyoi on 2023/11/14.
//

#ifndef MEDIASIMPLESERVER_RTMPHANDSHAKE_H
#define MEDIASIMPLESERVER_RTMPHANDSHAKE_H

#include "../../constant/Const.h"

namespace MS {

    class RtmpHandShake {
    public:
        enum class State{
            HANDSHAKE_C0C1,
            HANDSHAKE_S0S1S2,
            HANDSHAKE_C2,
            HANDSHAKE_DONE
        };

        RtmpHandShake(State state);
        virtual ~RtmpHandShake();

        int Parse(char* Buf,int BufLen,char* OutBuf,int OutBufLen);

        int BuildC0C1(char* Buf,int BufLen);

        bool isDone()const;
    private:
        State HandShakeState;
    };

} // MS

#endif //MEDIASIMPLESERVER_RTMPHANDSHAKE_H
