//
// Created by luyoi on 2023/11/14.
//

#include <random>
#include "RtmpHandShake.h"

namespace MS {
    RtmpHandShake::RtmpHandShake(RtmpHandShake::State state) {
        HandShakeState=state;
    }

    RtmpHandShake::~RtmpHandShake() {

    }

    int RtmpHandShake::Parse(char *Buf, int BufLen, char *OutBuf, int OutBufLen) {
        unsigned int pos=0;
        unsigned int res=0;
        std::random_device rd;

        if(HandShakeState==State::HANDSHAKE_S0S1S2){
            if(BufLen<(1+1536+1536)){
                return res;
            }
            if(Buf[0]!=RTMP_VERSION){
                spdlog::error("RTMP_VESION {} unsupported",RTMP_VERSION);
                return -1;
            }

            pos+=1+1536+1536;
            res=1536;//C2
            memcpy(OutBuf,Buf+1,1536);
            HandShakeState=State::HANDSHAKE_DONE;
        }else if (HandShakeState == State::HANDSHAKE_C0C1)
        {
            if (BufLen < 1537) { //c0c1
                return res;
            }
            else
            {
                if (Buf[0] != RTMP_VERSION) {
                    return -1;
                }

                pos += 1537;
                res = 1 + 1536 + 1536;
                memset(OutBuf, 0, 1537); //S0 S1 S2
                OutBuf[0] = RTMP_VERSION;

                char *p = OutBuf; p += 9;
                for (int i = 0; i < 1528; i++) {
                    *p++ = rd();
                }
                memcpy(p, Buf + 1, 1536);
                HandShakeState = State::HANDSHAKE_C2;
            }
        }else if (HandShakeState == State::HANDSHAKE_C2)
        {
            if (BufLen < 1536) { //c2
                return res;
            }
            else {
                pos = 1536;
                HandShakeState = State::HANDSHAKE_DONE;
            }
        }
        else {
            return -1;
        }

        return res;
    }

    int RtmpHandShake::BuildC0C1(char *Buf, int BufLen) {
        unsigned int size=1+1536;//C0C1
        memset(Buf,0,size);
        Buf[0]=RTMP_VERSION;
        std::random_device rd;
        unsigned char* ptr=(unsigned char*)Buf;
        ptr+=9;
        for(int i=0;i<1528;i++){
            *ptr++=rd();
        }
        return size;
    }

    bool RtmpHandShake::isDone() const {
        return HandShakeState==State::HANDSHAKE_DONE;
    }
} // MS