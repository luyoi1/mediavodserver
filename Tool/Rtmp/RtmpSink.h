//
// Created by luyoi on 2023/11/14.
//

#ifndef MEDIASIMPLESERVER_RTMPSINK_H
#define MEDIASIMPLESERVER_RTMPSINK_H
#include "../Amf/AmfTool.h"

#endif //MEDIASIMPLESERVER_RTMPSINK_H

#include <iostream>
#include <librtmp/amf.h>

namespace MS{
    class RtmpSink
    {
    public:
        RtmpSink() = default;
        virtual ~RtmpSink() = default;

        virtual bool SendMetaData(AmfObjMap metaData) { return true; };
        virtual bool SendMediaData(uint8_t type, uint64_t timestamp, std::shared_ptr<char> payload, uint32_t payload_size) = 0;
        virtual bool SendVideoData(uint64_t timestamp, std::shared_ptr<char> payload, uint32_t payload_size) = 0;
        virtual bool SendAudioData(uint64_t timestamp, std::shared_ptr<char> payload, uint32_t payload_size) = 0;

        virtual bool IsPlayer() { return false; }
        virtual bool IsPublisher() { return false; };
        virtual bool IsPlaying() { return false; }
        virtual bool IsPublishing() { return false; };


    };
}