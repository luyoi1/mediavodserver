//
// Created by luyoi on 2023/11/14.
//

#include "RtmpLinkState.h"

namespace MS {
    size_t RtmpLinkState::getPeerBandWidth() const {
        return PeerBandWidth;
    }

    size_t RtmpLinkState::getAckNowLedgement() const {
        return AckNowLedgement;
    }

    size_t RtmpLinkState::getMaxChunkSize() const {
        return MaxChunkSize;
    }

    size_t RtmpLinkState::getMaxGopCacheLen() const {
        return MaxGopCacheLen;
    }

    std::string_view RtmpLinkState::getUrl() const {
        return Url;
    }

    std::string_view RtmpLinkState::getTcUrl() const {
        return TcUrl;
    }

    std::string_view RtmpLinkState::getSwfUrl() const {
        return SwfUrl;
    }

    std::string_view RtmpLinkState::getApp() const {
        return App;
    }

    std::string_view RtmpLinkState::getStreamName() const {
        return StreamName;
    }

    std::string_view RtmpLinkState::getStreamPath() const {
        return StreamPath;
    }

    void RtmpLinkState::SetChunkSize(size_t size) {
        if (size > 0 && size <= 60000) {
            MaxChunkSize = size;
        }
    }

    void RtmpLinkState::SetGopCacheLen(size_t Len) {
        MaxGopCacheLen=Len;
    }

    void RtmpLinkState::SetPeerBandWidth(size_t size) {
        PeerBandWidth=size;
    }
} // MS