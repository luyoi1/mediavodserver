//
// Created by luyoi on 2023/11/13.
//

#ifndef MEDIASIMPLESERVER_RTMPMESSAGE_H
#define MEDIASIMPLESERVER_RTMPMESSAGE_H

#ifdef _WIN32
#include <WinSock2.h>
#else
#include <arpa/inet.h>
#endif

#include <librtmp/rtmp.h>
#include <iostream>
#include <memory>

namespace MS{



    enum class ChunkFormat{
        CHUNK_TYPE_0=11,
        CHUNK_TYPE_1=7,
        CHUNK_TYPE_2=3,
        CHUNK_TYPE_3=0
    };

    struct RtmpMessageHeader{
        unsigned char TimeStamp[3];
        unsigned char BodyLength[3];
        unsigned char TypeID;
        unsigned char StreamID[4];
    };



    class RtmpMessage {
    public:
        RtmpMessage();
        RtmpMessage(RtmpMessageHeader* head,ChunkFormat format);
        int GetHeadLen();
        bool isReady();
        void Clear();


        unsigned int GetTimeStamp();
        unsigned int TimeStamp;
        unsigned int BodyLength;
        unsigned char TypeID;
        unsigned int StreamID;
        unsigned int extend_timestamp;
        bool LongTimeFlag;
        unsigned long long LongTimeStamp;
        unsigned char ChunkStreamID;
        unsigned int BodyIndex;
        ChunkFormat HeadLen;
        std::shared_ptr<char> Payload;
    };
}




#endif //MEDIASIMPLESERVER_RTMPMESSAGE_H
