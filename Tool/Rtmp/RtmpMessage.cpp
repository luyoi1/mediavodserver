//
// Created by luyoi on 2023/11/13.
//


#include "RtmpMessage.h"

namespace MS{
    int RtmpMessage::GetHeadLen() {
        return (int)HeadLen;
    }

    RtmpMessage::RtmpMessage()
    :TimeStamp(0),BodyLength(0),TypeID(0),StreamID(0),extend_timestamp(0),LongTimeFlag(false),
    LongTimeStamp(0),ChunkStreamID(0),BodyIndex(0),HeadLen(ChunkFormat::CHUNK_TYPE_3),Payload(nullptr)
    {
    }

    RtmpMessage::RtmpMessage(RtmpMessageHeader *head, ChunkFormat format)
        :TimeStamp(0),BodyLength(0),TypeID(0),StreamID(0),extend_timestamp(0),LongTimeFlag(false),
    LongTimeStamp(0),ChunkStreamID(0),BodyIndex(0),HeadLen(ChunkFormat::CHUNK_TYPE_3),Payload(nullptr)
    {
        HeadLen=format;
        if(HeadLen==ChunkFormat::CHUNK_TYPE_3){
            return;
        }
        memcpy(&TimeStamp,head->TimeStamp,3);
        TimeStamp= ntohl(TimeStamp);
        TimeStamp=TimeStamp>>8;
        if(TimeStamp>=0x00FFFFFF){
            LongTimeFlag=true;
        }
        if(HeadLen==ChunkFormat::CHUNK_TYPE_2){
            return;
        }
        memcpy(&BodyLength,head->BodyLength,3);
        BodyLength= ntohl(BodyLength);
        BodyLength=BodyLength>>8;

        if(BodyLength>0){
            Payload.reset(new char[BodyLength],std::default_delete<char[]>());
        }

        memcpy(&TypeID,&head->TypeID,1);
        if(HeadLen==ChunkFormat::CHUNK_TYPE_1){
            return;
        }
        memcpy(&StreamID,head->StreamID,4);

    }

    unsigned int RtmpMessage::GetTimeStamp() {
        return LongTimeStamp;
    }

    bool RtmpMessage::isReady() {
        return BodyIndex==BodyLength&&BodyLength>0&&Payload!= nullptr;
    }

    void RtmpMessage::Clear() {
        BodyIndex=0;
        TimeStamp=0;
        LongTimeFlag=false;
        extend_timestamp=0;
        if(BodyLength>0) {
            Payload.reset(new char[BodyLength]);
        }
    }
}


