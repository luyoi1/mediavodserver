//
// Created by luyoi on 2023/10/21.
//

#include "Session.h"

#include <utility>

Session::Session(std::string_view Name):
    SessionName(Name)
{

}

Session::~Session() {

}


std::string_view Session::GetSessionName() {
    return std::string_view(this->SessionName);
}


void Session::AddGop(int16_t MediaID,std::shared_ptr<Sink> sink)
{
    std::lock_guard<std::mutex> lk(GopMapMtx);
    GopMap.emplace(MediaID,std::make_shared<MediaGopInfo>(sink));
}

void Session::RemoveGop(int16_t MediaID)
{
    if (!GopMap.contains(MediaID))
        return;
    std::lock_guard<std::mutex> lk(GopMapMtx);
    GopMap.erase(MediaID);
}



std::shared_ptr<MediaGopInfo> Session::GetGop(int16_t MediaID)
{
    if (!GopMap.contains(MediaID))
        return {};
    return GopMap[MediaID];
}

void Session::AddConn(int32_t id)
{
    if(ConnMap.contains(id))
        return;
    std::lock_guard<std::mutex> lk(ConnMapMtx);
    ConnMap[id]= true;
}

void Session::RemoveConn(int32_t id)
{
    if(!ConnMap.contains(id))
        return;
    std::lock_guard<std::mutex> lk(ConnMapMtx);
    ConnMap.erase(id);
    /*
    if(!ConnSize.load())
    {
        std::lock_guard<std::mutex> lk(GopMapMtx);
        for(auto&[key,val]:GopMap)
        {
            val->ClearGop();
        }
    }
    */
}

const std::map<int16_t, std::shared_ptr<MediaGopInfo>> &Session::GetGopMap() {
    return GopMap;
}


