//
// Created by luyoi on 2023/10/18.
//

#include "MediaSource.h"

MediaFrame::MediaFrame():
    buffer(new unsigned char[FRAME_MAX_SIZE]),dataLength(0),dataPoint(nullptr)
{
    memset(buffer.get(),0,FRAME_MAX_SIZE);
}

MediaFrame::~MediaFrame() {
    dataPoint= nullptr;
}

void MediaFrame::clear() {
    memset(buffer.get(),0,FRAME_MAX_SIZE);
    dataPoint= nullptr;
    dataLength=0;
}

MediaFrame::MediaFrame(size_t size)
    :buffer(new unsigned char[size]),dataLength(size),dataPoint(nullptr)
{

}

MediaSource::MediaSource(boost::asio::io_context& ioc_,std::string_view path):
    MediaFps(0),ioc(ioc_),FilePath(path),isStop(false)
{

}

MediaSource::~MediaSource() {

}

std::shared_ptr<MediaFrame> MediaSource::getFrameFromOutputQueue() {
    if(isStop){
        return {};
    }
    std::unique_lock<std::mutex> uql(MediaOutputMux);
    if(FrameOutputQueue.empty()){
        FrameCond.wait(uql,[this]{
            return isStop||!FrameOutputQueue.empty();
        });
    }
    if(isStop){
        return {};
    }
    auto lcSource=FrameOutputQueue.front();
    FrameOutputQueue.pop();
    return lcSource;
}

void MediaSource::putFrameToInputQueue(std::shared_ptr<MediaFrame> frame) {
    if(isStop){
        return;
    }
    std::unique_lock<std::mutex> uql(MediaInputMux);
    if(FrameInputQueue.size()>=FRAME_BUFFER_LENGTH){
        spdlog::warn("FRAME INPUT BUFFER FULL!Length is {}",FrameInputQueue.size());
    }
    frame->clear();
    FrameInputQueue.push(frame);
    ioc.post([this]{
        Handler(shared_from_this());
    });//加入处理事件
}

size_t MediaSource::getFps() const {
    return MediaFps;
}

std::string_view MediaSource::getSourceName() {
    return std::string_view(SourceName);
}

void MediaSource::setFps(size_t fps) {
    MediaFps=fps;
}

std::string_view MediaSource::getFilePath() {
    return FilePath;
}

void MediaSource::Stop() {
    isStop.store(true);
    FrameCond.notify_all();
}

void MediaSource::Start() {

}
