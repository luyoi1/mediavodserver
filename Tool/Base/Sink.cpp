//
// Created by luyoi on 2023/10/20.
//

#include "Sink.h"
#include <random>

Sink::Sink(std::shared_ptr<MediaSource> mediaSource, LoadType payloadType):
    SinkMediaSoucre(mediaSource),PayloadType(payloadType),isStop(true)
{
    SinkMediaSoucre->Start();
}

Sink::~Sink() {
    StopTimer();
    SinkMediaSoucre->Stop();
}

void Sink::StopTimer() {
    if(isStop)
        return;
    isStop.store(true);
    if(Timer.use_count()!=0){
        Timer->cancel();
        Timer.reset();
    }

}

void Sink::TimeoutHandler(boost::system::error_code ec,std::shared_ptr<Sink> node) {
    if(ec||isStop){
        return;
    }
    auto frame=SinkMediaSoucre->getFrameFromOutputQueue();
    if(frame == nullptr){
        return;
    }
    SaveFunc(frame);
    SinkMediaSoucre->putFrameToInputQueue(frame);
    //InitFrame(frame);
    if(isStop){
        return;
    }
    Timer->expires_from_now(GetTimeInterval());
    Timer->async_wait(std::bind(&Sink::TimeoutHandler,this,std::placeholders::_1,shared_from_this()));
}

void Sink::RunTimer() {
    if(!isStop)
        return;
    if(Timer.use_count()!=0){
        Timer->cancel();
        Timer.reset();
    }
    //Timestamp= (unsigned int)time(nullptr);
    isStop.store(false);
    Timer=std::make_shared<boost::asio::steady_timer>(IOContextPool::GetInstance()->GetContext()
            ,GetTimeInterval());
    Timer->async_wait(std::bind(&Sink::TimeoutHandler,this,std::placeholders::_1,shared_from_this()));
}


/*
void Sink::SetSendFunc(const SendFunc &func) {
    SendPack=func;
}*/



void Sink::SetSaveFunc(const Sink::SaveGop func) {
    SaveFunc=func;
}

Sink::SendFrameFunc Sink::GetSendFrameFun() {
    auto node=shared_from_this();
    return std::bind([node](std::shared_ptr<MediaFrame> frame,SendRtpPack func){
        node->InitFrame(frame, func, nullptr);
    },std::placeholders::_1,std::placeholders::_2);
}

std::chrono::milliseconds Sink::GetTimeInterval() {
    return std::chrono::milliseconds(1000/SinkMediaSoucre->getFps());
}

uint8_t Sink::getPayloadType() const {
    return PayloadType;
}

uint8_t Sink::getMarker() const {
    return Marker;
}





