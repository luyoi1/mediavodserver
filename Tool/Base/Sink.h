//
// Created by luyoi on 2023/10/20.
//

#ifndef MEDIASIMPLESERVER_SINK_H
#define MEDIASIMPLESERVER_SINK_H

#include<functional>
#include<map>
#include "../Rtsp/Rtp.h"
#include "MediaSource.h"
#include "../IOContextPool.h"
#include "../../constant/Const.h"


class Sink: public std::enable_shared_from_this<Sink>{
public:
    typedef std::function<void(std::shared_ptr<RtpPacket>)> SendRtpPack;
    //typedef std::function<void(std::shared_ptr<RtpPacket>&,unsigned short,unsigned short)> SendFunc;
    typedef std::function<void (std::shared_ptr<MediaFrame>)> SaveGop;
    typedef std::function<void (std::shared_ptr<MediaFrame>,SendRtpPack)> SendFrameFunc;
public:
    Sink(std::shared_ptr<MediaSource> mediaSource,LoadType payloadType);
    virtual ~Sink();
    void RunTimer();
    void StopTimer();
    std::chrono::milliseconds GetTimeInterval();
    //void SetSendFunc(const SendFunc& func);
    void SetSaveFunc(const SaveGop func);

    uint8_t getPayloadType() const;

    uint8_t getMarker() const;

    SendFrameFunc GetSendFrameFun();

    virtual std::string GetMediaDescription(unsigned short port)=0;
    virtual std::string GetAttribute()=0;
    virtual uint32_t CheckTime(char *dataPtr) =0;
    virtual void CheckIndex(char* dataPtr,const uint32_t nowSize,uint32_t* PreIndex, std::map<uint32_t, uint32_t>& MetaTimeMap)=0;
    virtual uint16_t InitFrame(std::shared_ptr<MediaFrame> frame, SendRtpPack func, uint32_t *TimeStamp) =0;
private:
    void TimeoutHandler(boost::system::error_code ec,std::shared_ptr<Sink> node);

protected:
    //virtual void SendNode(std::shared_ptr<RtpPacket>& node)=0;

    std::atomic_bool isStop;
    //SendFunc SendPack;
    SaveGop  SaveFunc;
    std::shared_ptr<MediaSource> SinkMediaSoucre;
    std::shared_ptr<boost::asio::steady_timer> Timer;
    uint8_t PayloadType;
    uint8_t Marker;


};


#endif //MEDIASIMPLESERVER_SINK_H
