//
// Created by luyoi on 2023/10/18.
//

#ifndef MEDIASIMPLESERVER_MEDIASOURCE_H
#define MEDIASIMPLESERVER_MEDIASOURCE_H

#include<iostream>
#include<queue>
#include<mutex>
#include<spdlog/spdlog.h>
#include<boost/asio.hpp>

//视频相关常量
constexpr unsigned int FRAME_MAX_SIZE=1024*200;
constexpr unsigned short FRAME_BUFFER_LENGTH=10;


//视频帧
class MediaFrame{
public:
    MediaFrame();
    MediaFrame(size_t size);
    ~MediaFrame();
    void clear();

    int dataLength;
    std::shared_ptr<unsigned char> buffer;//用于存储数据,负责数据的生命周期
    unsigned char*  dataPoint;//指向有效数据，此指针不负责数据的生命周期
};

//媒体处理
class MediaSource:public std::enable_shared_from_this<MediaSource> {
public:
    MediaSource(boost::asio::io_context& ioc,std::string_view path);
    virtual ~MediaSource();

    std::shared_ptr<MediaFrame> getFrameFromOutputQueue();//从输出队列获取帧
    void putFrameToInputQueue(std::shared_ptr<MediaFrame> frame);//向输入队列推送帧
    size_t getFps()const;//获取帧数
    std::string_view getSourceName();//获取该媒体流的名称
    std::string_view getFilePath();
    void Stop();
    virtual void Start();
protected:
    virtual void Handler(std::shared_ptr<MediaSource> node)=0;//媒体流处理
    void setFps(size_t fps);

    std::queue<std::shared_ptr<MediaFrame>> FrameInputQueue;
    std::queue<std::shared_ptr<MediaFrame>> FrameOutputQueue;
    size_t MediaFps;
    std::mutex MediaInputMux;
    std::mutex MediaOutputMux;
    std::string SourceName;
    std::string FilePath;
    boost::asio::io_context& ioc;
    std::atomic_bool isStop;
    std::condition_variable FrameCond;
};


#endif //MEDIASIMPLESERVER_MEDIASOURCE_H
