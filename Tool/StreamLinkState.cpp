//
// Created by luyoi on 2023/11/21.
//

#include "StreamLinkState.h"

#include <utility>

StreamLinkState::StreamLinkState(boost::asio::io_context& ioc_,std::shared_ptr<MediaGopInfo> gopPtr,uint16_t id)
        :ioc(ioc_),isStop(true),GopPtr(gopPtr),MediaID(id),CsrcLen(0),Extension(0),Padding(0),
        Version(RTP_VESION),PayloadType(GopPtr->GetSink()->getPayloadType()),Marker(GopPtr->GetSink()->getMarker()),CSeq(1),
        SSRC(0),Timestamp(0),GopIndex(0)
{
    std::mt19937 engine(std::random_device{}());
    std::uniform_int_distribution<unsigned int> distribution;
    SSRC = distribution(engine);
    TimeInterval=GopPtr->GetSink()->GetTimeInterval();

    auto node=GopPtr->GetSink();
    auto InitFunc=[node](std::shared_ptr<MediaFrame> frame,SendRtpPack func,uint32_t* time)->uint16_t {
        return node->InitFrame(frame, func, time);
    };
    SetInitFunc(InitFunc);
}

StreamLinkState::~StreamLinkState(){
    StopTimer();
}

void StreamLinkState::SendRtpPacket(std::shared_ptr<RtpPacket> packet){
    if(isStop.load()){
        return;
    }
    RtpHeader* rtpHeader = packet->RtpHead;
    rtpHeader->csrcLen = CsrcLen;
    rtpHeader->extension = Extension;
    rtpHeader->padding = Padding;
    rtpHeader->version = Version;
    rtpHeader->payloadType = PayloadType;
    rtpHeader->marker = Marker;
    rtpHeader->seq = htons(CSeq);
    rtpHeader->timestamp = htonl(Timestamp);
    rtpHeader->ssrc = htonl(SSRC);


    SendPacket(packet,RtpChannel,RemoteLocalRtpPort);
}

void StreamLinkState::SetChannel(uint16_t rtpChann){
    RtpChannel=rtpChann;
    RtcpChannel=rtpChann+1;
}
void StreamLinkState::SetPort(uint16_t rtpPort){
    RemoteLocalRtpPort=rtpPort;
    RemoteLocalRtcpPort=rtpPort+1;
}
void StreamLinkState::SetInitFunc(InitRtpPacket func){
    InitFunc=std::move(func);
}

void StreamLinkState::SetSendFun(SendFunc func){
    SendPacket=std::move(func);
}

void StreamLinkState::RunTimer(){
    isStop.store(false);
    Timer=std::make_shared<boost::asio::steady_timer>(ioc,TimeInterval);
    Timer->async_wait(std::bind(&StreamLinkState::TimeOutHandler,this,std::placeholders::_1));
}
void StreamLinkState::StopTimer(){
    if(isStop.load()){
        return;
    }
    isStop.store(true);
    if(Timer.use_count()){
        Timer->cancel();
        Timer.reset();
    }
}

void StreamLinkState::TimeOutHandler(boost::system::error_code ec) {
    if(ec||isStop){
        return;
    }
    auto frame=GopPtr->GetGop(GopIndex);
    if(frame == nullptr){
        return;
    }
    GopIndex++;
    CSeq+=InitFunc(frame,std::bind(&StreamLinkState::SendRtpPacket,this,std::placeholders::_1),&Timestamp);//传入发送函数，发送
    if(isStop){
        return;
    }
    Timer->expires_from_now(TimeInterval);
    Timer->async_wait(std::bind(&StreamLinkState::TimeOutHandler,this,std::placeholders::_1));
}

const std::shared_ptr<MediaGopInfo> &StreamLinkState::GetGop() {
    return GopPtr;
}



uint16_t StreamLinkState::getRtpChannel() const {
    return RtpChannel;
}

uint16_t StreamLinkState::getRtcpChannel() const {
    return RtcpChannel;
}

uint16_t StreamLinkState::getRemoteLocalRtpPort() const {
    return RemoteLocalRtpPort;
}

uint16_t StreamLinkState::getRemoteLocalRtcpPort() const {
    return RemoteLocalRtcpPort;
}
