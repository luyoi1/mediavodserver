//
// Created by luyoi on 2023/11/21.
//

#ifndef MEDIASIMPLESERVER_STREAMLINKSTATE_H
#define MEDIASIMPLESERVER_STREAMLINKSTATE_H

#include <boost/asio.hpp>
#include "Base/Sink.h"
#include "../Net/Tcp_Connection.h"
#include "MediaGop.h"

class StreamLinkState{
    typedef std::function<void(std::shared_ptr<RtpPacket>)> SendRtpPack;
    typedef std::function<uint16_t (std::shared_ptr<MediaFrame>,SendRtpPack,uint32_t*)> InitRtpPacket;
    typedef std::function<void(std::shared_ptr<RtpPacket>,uint16_t,uint16_t)> SendFunc;
public:
    StreamLinkState(boost::asio::io_context& ioc_,std::shared_ptr<MediaGopInfo> gopPtr,uint16_t id);
    ~StreamLinkState();
    void SetChannel(uint16_t rtpChann);
    void SetPort(uint16_t rtpPort);
    void SetInitFunc(InitRtpPacket func);
    void SetSendFun(SendFunc func);
    void SendRtpPacket(std::shared_ptr<RtpPacket> packet);
    void RunTimer();
    void StopTimer();
    const std::shared_ptr<MediaGopInfo>& GetGop();



    uint16_t getRtpChannel() const;
    uint16_t getRtcpChannel() const;
    uint16_t getRemoteLocalRtpPort() const;
    uint16_t getRemoteLocalRtcpPort() const;

private:
    void TimeOutHandler(boost::system::error_code ec);


    boost::asio::io_context& ioc;
    std::chrono::milliseconds TimeInterval;
    std::shared_ptr<MediaGopInfo> GopPtr;
    std::shared_ptr<boost::asio::steady_timer> Timer;
    std::atomic_bool isStop;
    InitRtpPacket InitFunc;
    SendFunc SendPacket;
    uint16_t MediaID;
    int32_t GopIndex;
    //连接的属性
    uint8_t CsrcLen;
    uint8_t Extension;
    uint8_t Padding;
    uint8_t Version;
    uint8_t PayloadType;
    uint8_t Marker;
    uint16_t CSeq;
    uint32_t Timestamp;
    uint32_t SSRC;

    uint16_t RtpChannel;
    uint16_t RtcpChannel;
    uint16_t RemoteLocalRtpPort;
    uint16_t RemoteLocalRtcpPort;
};


#endif //MEDIASIMPLESERVER_STREAMLINKSTATE_H
