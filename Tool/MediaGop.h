//
// Created by luyoi on 2023/11/19.
//

#ifndef MEDIASIMPLESERVER_MEDIAGOP_H
#define MEDIASIMPLESERVER_MEDIAGOP_H

#include <map>
#include "./Base/MediaSource.h"
#include "./Base/Sink.h"


class MediaGopInfo
{
public:
	MediaGopInfo(std::shared_ptr<Sink>& sink);

	void SaveGop(std::shared_ptr<MediaFrame> mediaframe);
    std::shared_ptr<MediaFrame> GetGop(int32_t GopIndex);
	int32_t GetStartGopIndex(uint32_t TimeStampOffest);
	void ClearGop();
    std::shared_ptr<Sink> GetSink();
    //Sink::TimeHandler GetFunc();
private:
	void StartReadMedia()const;
	void StopReadMedia()const;


	std::mutex GopMtx;
	std::deque<std::shared_ptr<MediaFrame>> GopMem;
	std::condition_variable GopCond;
	std::atomic_uint32_t GopMemSize;
	std::atomic_uint32_t GopWaitSize;
	std::map<uint32_t, uint32_t> GopMetaTimeMap;
	uint32_t preMetaIndex;
	uint32_t TimeStampOffest;
	std::shared_ptr<Sink> MediaSinkInfo;
};


#endif //MEDIASIMPLESERVER_MEDIAGOP_H
