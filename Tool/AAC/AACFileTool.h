//
// Created by luyoi on 2023/10/20.
//

#ifndef MEDIASIMPLESERVER_AACFILETOOL_H
#define MEDIASIMPLESERVER_AACFILETOOL_H

#endif //MEDIASIMPLESERVER_AACFILETOOL_H
#include<iostream>
#include<array>

static std::array<unsigned int,16> AACSampleRate{
    97000, 88200, 64000, 48000,
    44100, 32000, 24000, 22050,
    16000, 12000, 11025, 8000,
    7350, 0, 0, 0 /*reserved */
};