//
// Created by luyoi on 2023/10/19.
//

#include "AACMediaSource.h"
#include <filesystem>

AACMediaSource::AACMediaSource(boost::asio::io_context &ioc, const std::filesystem::path& FileName):
        MediaSource(ioc,FileName.string()),FileStream(std::make_shared<std::fstream>(FileName,std::ios::in|std::ios::binary))
{
    SourceName=FileName.filename().string();
    if(!FileStream->is_open()){
        spdlog::error("AAC MediaSource Init Error!{} Not Find",SourceName);
        return;
    }
    setFps(43);

}

AACMediaSource::~AACMediaSource() {
    if(FileStream->is_open()){
        FileStream->close();
    }
}

void AACMediaSource::Handler(std::shared_ptr<MediaSource> node) {
    if(isStop){
        return;
    }
    std::shared_ptr<MediaFrame> Frame;
    {
        std::unique_lock<std::mutex> uql(this->MediaInputMux);
        if(FrameInputQueue.empty()) {
            spdlog::warn("AAC FrameInputQueue is Empty!But is Post Task!");
            return;
        }
        Frame=FrameInputQueue.front();
        FrameInputQueue.pop();
    }


    Frame->dataLength= getFrameFromAACFile(Frame->buffer.get(),FRAME_MAX_SIZE);
    if(Frame->dataLength<0){
        spdlog::error("GetFrameFromAACFile Read Error!");
        Frame->clear();
    }
    Frame->dataPoint=Frame->buffer.get();

    std::unique_lock<std::mutex> uql(this->MediaOutputMux);
    FrameOutputQueue.push(Frame);
    FrameCond.notify_one();
}

bool AACMediaSource::parseAdtsHeader(const unsigned char *inputBuffer, struct AdtsHeader *result) {
    memset(result,0,sizeof(AdtsHeader));

    if ((inputBuffer[0] == 0xFF)&&((inputBuffer[1] & 0xF0) == 0xF0))
    {
        result->id = ((unsigned int) inputBuffer[1] & 0x08) >> 3;
        result->layer = ((unsigned int) inputBuffer[1] & 0x06) >> 1;
        result->protectionAbsent = (unsigned int) inputBuffer[1] & 0x01;
        result->profile = ((unsigned int) inputBuffer[2] & 0xc0) >> 6;
        result->samplingFreqIndex = ((unsigned int) inputBuffer[2] & 0x3c) >> 2;
        result->privateBit = ((unsigned int) inputBuffer[2] & 0x02) >> 1;
        result->channelCfg = ((((unsigned int) inputBuffer[2] & 0x01) << 2) | (((unsigned int) inputBuffer[3] & 0xc0) >> 6));
        result->originalCopy = ((unsigned int) inputBuffer[3] & 0x20) >> 5;
        result->home = ((unsigned int) inputBuffer[3] & 0x10) >> 4;
        result->copyrightIdentificationBit = ((unsigned int) inputBuffer[3] & 0x08) >> 3;
        result->copyrightIdentificationStart = (unsigned int) inputBuffer[3] & 0x04 >> 2;
        result->aacFrameLength = (((((unsigned int) inputBuffer[3]) & 0x03) << 11) |
                               (((unsigned int)inputBuffer[4] & 0xFF) << 3) |
                               ((unsigned int)inputBuffer[5] & 0xE0) >> 5) ;
        result->adtsBufferFullness = (((unsigned int) inputBuffer[5] & 0x1f) << 6 |
                                   ((unsigned int) inputBuffer[6] & 0xfc) >> 2);
        result->numberOfRawDataBlockInFrame = ((unsigned int) inputBuffer[6] & 0x03);

        return true;
    }
    else
    {
        spdlog::error("failed to parse adts header");
        return false;
    }
}

int AACMediaSource::getFrameFromAACFile(unsigned char *FrameBuffer, int BufferSize) {
    if (FrameBuffer== nullptr) {
        return -1;
    }

    //unsigned char tmpBuf[7]{};
    int ret = FileStream->read((char*)FrameBuffer,7).gcount();
    if(ret <= 0)
    {
        FileStream->seekg(0,std::ios::beg);
        spdlog::error("AAC Read Error!");
        return -1;
    }

    if(!parseAdtsHeader(FrameBuffer, &AdtsHead))
    {
        FileStream->seekg(0,std::ios::beg);
        spdlog::error("parseAdtsHeader error!");
        return -1;
    }

    if(AdtsHead.aacFrameLength-7 > BufferSize){
        FileStream->seekg(0,std::ios::beg);
        spdlog::error("AACFrame Length to long!");
        return -1;
    }

    ret = FileStream->read((char*)FrameBuffer,AdtsHead.aacFrameLength-7).gcount();
    if(ret < 0)
    {
        FileStream->seekg(0,std::ios::beg);
        spdlog::error("AAC Read Error!");
        return -1;
    }
    return AdtsHead.aacFrameLength-7;
}

std::shared_ptr<MediaSource> AACMediaSource::Create(boost::asio::io_context &ioc, const std::string &FileName) {
    return std::make_shared<AACMediaSource>(ioc,FileName);
}

void AACMediaSource::Start() {
    for(size_t i=0;i<FRAME_BUFFER_LENGTH;i++){
        this->putFrameToInputQueue(std::make_shared<MediaFrame>());
    }
}
