//
// Created by luyoi on 2023/10/20.
//

#include "AACSink.h"

AACSink::AACSink(std::shared_ptr<MediaSource> mediaSource):
        Sink(mediaSource,LoadType::AAC), SampleRate(44100), Channels(2),
        Fps(mediaSource->getFps())
{
    Marker=1;
}

AACSink::~AACSink() {

}

std::string AACSink::GetMediaDescription(unsigned short port) {
    auto str=fmt::format("m=audio {} RTP/AVP {}",port,PayloadType);
    return str;
}

std::string AACSink::GetAttribute() {
    auto str=fmt::format("a=rtpmap:97 mpeg4-generic/{}/{}\r\n", SampleRate, Channels);

    uint8_t index = 0;
    for (index = 0; index < 16; index++)
    {
        if (AACSampleRate[index] == SampleRate)
            break;
    }
    if (index == 16)
        return "";

    unsigned char profile=1;

    auto configStr=fmt::format("{:02x}{:02x}", (uint8_t)((profile+1) << 3)|(index >> 1),
            (uint8_t)((index << 7)|(Channels<< 3)));
    str+=fmt::format("a=fmtp:{} profile-level-id=1;"
                     "mode=AAC-hbr;"
                     "sizelength=13;indexlength=3;indexdeltalength=3;"
                     "config={:04}",PayloadType,stoi(configStr));

    return str;
}

uint16_t AACSink::InitFrame(std::shared_ptr<MediaFrame> frame, SendRtpPack func, uint32_t *TimeStamp) {
    if(isStop){
        return 0;
    }
    int FrameSize=frame->dataLength;
    auto rtpPack=std::make_shared<RtpPacket>(FrameSize+4);
    rtpPack->RtpHead->payload[0]=0x00;
    rtpPack->RtpHead->payload[1]=0x10;
    rtpPack->RtpHead->payload[2]=(FrameSize&0x1FE0)>>5;
    rtpPack->RtpHead->payload[3]=(FrameSize&0x1F)<<3;
    memcpy(rtpPack->RtpHead->payload+4,frame->dataPoint,FrameSize);//迁移数据

    func(rtpPack);//发送RTP包

    //CSeq++;

    //Timestamp+=SampleRate*(1000/Fps)/1000;
    //使用完的帧包放回输入队列
    //SinkMediaSoucre->putFrameToInputQueue(frame);
    return 1;
}

/*
void AACSink::SendNode(std::shared_ptr<RtpPacket> &node) {
    //SendPack(node,RtpChannel,RemoteLocalRtpPort);
}
 */

std::shared_ptr<Sink> AACSink::Create(std::shared_ptr<MediaSource> mediaSource) {
    return std::make_shared<AACSink>(mediaSource);
}

uint32_t AACSink::CheckTime(char *dataPtr) {
    return SampleRate*(1000/Fps)/1000;
}

void AACSink::CheckIndex(char *dataPtr, const uint32_t nowSize, uint32_t *PreIndex,std::map<uint32_t, uint32_t> &MetaTimeMap) {
    *PreIndex=nowSize;
    return;
}
