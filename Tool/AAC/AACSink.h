//
// Created by luyoi on 2023/10/20.
//

#ifndef MEDIASIMPLESERVER_AACSINK_H
#define MEDIASIMPLESERVER_AACSINK_H

#include "../Base/Sink.h"
#include "AACFileTool.h"

class AACSink: public Sink{
public:
    static std::shared_ptr<Sink> Create(std::shared_ptr<MediaSource> mediaSource);
    AACSink(std::shared_ptr<MediaSource> mediaSource);
    virtual ~AACSink();

    virtual std::string GetMediaDescription(unsigned short port);
    virtual std::string GetAttribute();

    virtual uint32_t CheckTime(char *dataPtr);

    virtual void CheckIndex(char *dataPtr, const uint32_t nowSize, uint32_t *PreIndex,std::map<uint32_t, uint32_t> &MetaTimeMap);

protected:
    virtual uint16_t InitFrame(std::shared_ptr<MediaFrame> frame, SendRtpPack func, uint32_t *TimeStamp);
    //virtual void SendNode(std::shared_ptr<RtpPacket>& node);
private:
    unsigned int SampleRate;//采样率
    unsigned int Channels;//通道数
    int Fps;
};


#endif //MEDIASIMPLESERVER_AACSINK_H
