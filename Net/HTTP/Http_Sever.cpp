//
// Created by luyoi on 2023/10/14.
//

#include "Http_Sever.h"

#include "Http_Connection.h"

Http_Sever::Http_Sever(boost::asio::io_context &ioc_, std::string ip, unsigned short port)
    : Tcp_Server(ioc_,ip,port)
{

}

Http_Sever::~Http_Sever() {

}

void Http_Sever::Start() {
    isStop.store(false);
    auto conn=std::make_shared<Http_Connection>(ioc,shared_from_this());
    ServerAccept.async_accept(conn->GetSock(),std::bind(&Http_Sever::HandleAccept,this,std::placeholders::_1,conn));
}

void Http_Sever::HandleAccept(boost::system::error_code ec, std::shared_ptr<Tcp_Connection> conn) {
    if(ec){
        if(isStop.load()){
            return;
        }
        spdlog::error("Accept Error,Error code is {},Error is {}",ec.value(),ec.message());
        Stop();
        return;
    }
    AddConn(conn);
    std::reinterpret_pointer_cast<Http_Connection>(conn)->Start();
    auto newConn=std::make_shared<Http_Connection>(IOContextPool::GetInstance()->GetContext(),shared_from_this());
    ServerAccept.async_accept(newConn->GetSock(),std::bind(&Http_Sever::HandleAccept,this,std::placeholders::_1,newConn));
}


