//
// Created by luyoi on 2023/10/14.
//

#ifndef MEDIASIMPLESERVER_HTTP_CONNECTION_H
#define MEDIASIMPLESERVER_HTTP_CONNECTION_H

#include <iostream>
#include <boost/asio.hpp>
#include <boost/beast.hpp>
#include <spdlog/spdlog.h>
#include <filesystem>
#include <fstream>
#include <boost/uuid/random_generator.hpp>
#include<boost/uuid/uuid_io.hpp>
#include<queue>
#include "../Tcp_Connection.h"


class Http_Connection:public Tcp_Connection
{
public:
    Http_Connection(boost::asio::io_context& ioc,std::shared_ptr<Tcp_Server> server);
    ~Http_Connection();

    virtual void Start() override;

protected:
    void HandleSend(boost::system::error_code ec, std::shared_ptr<Tcp_Connection> sharedNode) override;

    void HandleRecv(boost::system::error_code ec, size_t RecvByteSize,std::shared_ptr<Tcp_Connection> sharedNode) override;

private:
    void ConnSend(std::shared_ptr<boost::beast::http::response<boost::beast::http::dynamic_body>> resp);

    void SendHlsFunc(boost::system::error_code ec, std::size_t size, std::shared_ptr<std::fstream> stream);
    void DealReq();


    std::atomic_bool isSend;
    boost::beast::http::request<boost::beast::http::dynamic_body> req;
    boost::beast::flat_buffer buffer;
    std::queue<std::shared_ptr<boost::beast::http::response<boost::beast::http::dynamic_body>>> SendQue;
};


#endif //MEDIASIMPLESERVER_HTTP_CONNECTION_H
