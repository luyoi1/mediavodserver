//
// Created by luyoi on 2023/10/14.
//

#ifndef MEDIASIMPLESERVER_HTTP_SEVER_H
#define MEDIASIMPLESERVER_HTTP_SEVER_H

#include "../Tcp_Server.h"


class Http_Sever:public Tcp_Server{
public:
    Http_Sever(boost::asio::io_context& ioc_,std::string ip,unsigned short port);
    ~Http_Sever();
    virtual void Start() override;
protected:
    void HandleAccept(boost::system::error_code ec, std::shared_ptr<Tcp_Connection> conn) override;
};


#endif //MEDIASIMPLESERVER_HTTP_SEVER_H
