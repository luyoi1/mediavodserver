//
// Created by luyoi on 2023/10/14.
//

#include "Http_Connection.h"

const char* FilePath="../data";

Http_Connection::Http_Connection(boost::asio::io_context& ioc,std::shared_ptr<Tcp_Server> server)
:Tcp_Connection(ioc,server,0),buffer(8192)
{

}
Http_Connection::~Http_Connection(){

}


void Http_Connection::Start(){
    boost::beast::http::async_read(sock,buffer,req,
                                   std::bind(&Http_Connection::HandleRecv,this,std::placeholders::_1,std::placeholders::_2,shared_from_this()));
}



void Http_Connection::HandleRecv(boost::system::error_code ec, size_t RecvByteSize,std::shared_ptr<Tcp_Connection> sharedNode){
    if(!ec){
        DealReq();
        buffer.clear();
        req.clear();
        boost::beast::http::async_read(sock,buffer,req,
                                       std::bind(&Http_Connection::HandleRecv,this,std::placeholders::_1,std::placeholders::_2,sharedNode));
        return;
    }
    this->Stop();
}

void Http_Connection::SendHlsFunc(boost::system::error_code ec, std::size_t size, std::shared_ptr<std::fstream> stream){
    if(!ec){
        spdlog::info("write size is {}",size);
        if(!isSend.load()){
            return;
        }
        std::string str{};
        str.resize(5000);
        if(stream->eof()){
            isSend.store(false);
            return;
        }
        auto readSize=stream->read(str.data(),str.size()).gcount();
        if(readSize==0){
            isSend.store(false);
            return;
        }
        //Sleep(10);
        this->sock.async_send(boost::asio::buffer(str.c_str(),str.size())
                ,std::bind(&Http_Connection::SendHlsFunc, this, std::placeholders::_1, std::placeholders::_2, stream));
        return;
    }
    spdlog::error("send video error, error is {}",ec.what());
    this->Stop();
}

void Http_Connection::DealReq(){
    boost::beast::http::response<boost::beast::http::dynamic_body> resp;
    resp.version(req.version());
    if(req.method()!=boost::beast::http::verb::get){
        resp.keep_alive(false);
        resp.set(boost::beast::http::field::server, "Beast");
        resp.result(boost::beast::http::status::bad_request);
        resp.content_length(0);
        auto node=std::make_shared<boost::beast::http::response<boost::beast::http::dynamic_body>>(std::move(resp));
        ConnSend(node);
    }
    if(req.method()==boost::beast::http::verb::get&&req.target()=="/test.flv"&&!isSend.load()){
        resp.result(boost::beast::http::status::ok);
        resp.set(boost::beast::http::field::content_length,"-1");
        resp.set(boost::beast::http::field::content_type,"video/x-flv");
        resp.set(boost::beast::http::field::access_control_allow_origin,"*");
        resp.set(boost::beast::http::field::pragma,"no-cache");
        resp.set(boost::beast::http::field::expires,"-1");
        auto node=std::make_shared<boost::beast::http::response<boost::beast::http::dynamic_body>>(std::move(resp));
        ConnSend(node);
        isSend.store(true);

        std::string target(req.target());
        ioc.post([this,target]{
            std::filesystem::path path1(std::string(FilePath)+target);
            if(!std::filesystem::exists(path1)){
                isSend.store(false);
                return;
            }
            std::shared_ptr<std::fstream> videoStream=std::make_shared<std::fstream>(path1, std::ios::in|std::ios::binary);
            std::string str{};
            str.resize(5000);
            auto readSize=videoStream->read(str.data(),str.size()).gcount();
            if(readSize==0){
                isSend.store(false);
                return;
            }
            this->sock.async_send(boost::asio::buffer(str.c_str(),str.size())
                    ,std::bind(&Http_Connection::SendHlsFunc, this, std::placeholders::_1, std::placeholders::_2, videoStream));
        });
        return;
    }

    std::string strPath=std::string(FilePath)+std::string(req.target());
    std::filesystem::path path1(strPath);
    if(!std::filesystem::exists(path1)){
        resp.result(boost::beast::http::status::not_found);
        resp.content_length(0);
        auto node=std::make_shared<boost::beast::http::response<boost::beast::http::dynamic_body>>(std::move(resp));
        ConnSend(node);
        return;
    }
    resp.result(boost::beast::http::status::ok);
    if(path1.filename().extension()==".m3u8"){
        resp.set(boost::beast::http::field::content_type,"application/vnd.apple.mpegurl; charset=utf-8");
    }else if(path1.filename().extension()==".ts"){
        resp.set(boost::beast::http::field::content_type,"video/mp2t; charset=utf-8");
    }else{
        resp.set(boost::beast::http::field::content_type,"text/html");
    }
    resp.keep_alive(true);
    resp.set(boost::beast::http::field::access_control_allow_origin,"*");
    resp.set(boost::beast::http::field::server,"beast");

    std::fstream videoStream(path1,std::ios::in|std::ios::binary);
    boost::beast::ostream(resp.body())<<videoStream.rdbuf();
    resp.content_length(resp.body().size());
    auto node=std::make_shared<boost::beast::http::response<boost::beast::http::dynamic_body>>(std::move(resp));
    ConnSend(node);
}


void Http_Connection::ConnSend(std::shared_ptr<boost::beast::http::response<boost::beast::http::dynamic_body>> resp){
    if(isSend.load()){
        return;
    }
    std::lock_guard<std::mutex> lg(SendMtx);
    SendQue.push(resp);
    if(SendQue.size()>1){
        return;
    }
    boost::beast::http::async_write(sock,*resp,
                                    std::bind(&Http_Connection::HandleSend,this,std::placeholders::_1,shared_from_this()));
}

void Http_Connection::HandleSend(boost::beast::error_code ec, std::shared_ptr<Tcp_Connection> sharedNode){
    if(ec){
        spdlog::error("write error,error is {}",ec.what());
        this->Stop();
    }
    std::lock_guard<std::mutex> lg(SendMtx);
    SendQue.pop();
    if(SendQue.empty()){
        return;
    }
    auto resp=SendQue.front();
    boost::beast::http::async_write(sock,*resp,
                                    std::bind(&Http_Connection::HandleSend,this,std::placeholders::_1,sharedNode));
}


