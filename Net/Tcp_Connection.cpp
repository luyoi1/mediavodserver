//
// Created by luyoi on 2023/11/15.
//

#include "Tcp_Connection.h"
#include "Tcp_Server.h"

DataNode::DataNode(size_t size)
        :Data(new char[size]),DataSize(size),DataIndex(0){

}

DataNode::DataNode(std::shared_ptr<char> data,size_t size)
        :Data(data),DataSize(size),DataIndex(size){

}

void DataNode::Clear(){
    memset(Data.get(),0, DataSize);
    DataIndex=0;
}

void DataNode::ReSize(size_t size){
    if(size>DataSize){
        Data.reset(new char[size]);
        DataSize=size;
        DataIndex=0;
    }
    return;
}

Tcp_Connection::Tcp_Connection(boost::asio::io_context& ioc_,std::shared_ptr<Tcp_Server> server,size_t BufSize)
        :ioc(ioc_),sock(ioc),Buf(BufSize),isStop(true),Server(server){
    std::mt19937 engine(std::random_device{}());
    std::uniform_int_distribution<unsigned int> distribution;
    ConnID = distribution(engine);
}

Tcp_Connection::~Tcp_Connection(){
    this->Stop();
}

boost::asio::ip::tcp::socket& Tcp_Connection::GetSock(){
    return sock;
}

size_t Tcp_Connection::GetID() const{
    return ConnID;
}

void Tcp_Connection::Start(){
    sock.async_read_some(boost::asio::buffer(Buf.Data.get(),Buf.DataSize),
                         std::bind(&Tcp_Connection::HandleRecv,this,std::placeholders::_1,std::placeholders::_2,shared_from_this()));
    isStop.store(false);
}
void Tcp_Connection::Stop(){
    if(isStop.load()){
        return;
    }
    isStop.store(true);
    if(sock.is_open()){
        sock.cancel();
        sock.close();
    }
    Server->DelConn(ConnID);
}
void Tcp_Connection::Send(DataNode& node){
    std::lock_guard<std::mutex> lk(SendMtx);

    SendQue.push(node);

    if(SendQue.size()>1){
        return;
    }
    auto& Fnode=SendQue.front();
    sock.async_send(boost::asio::buffer(Fnode.Data.get(),Fnode.DataIndex),
                    std::bind(&Tcp_Connection::HandleSend,this,std::placeholders::_1,shared_from_this()));
}
void Tcp_Connection::Send(std::shared_ptr<char> data,size_t dataLen){
    DataNode node(data,dataLen);
    this->Send(node);
}
void Tcp_Connection::Send(std::string data){
    DataNode node(data.size());
    memcpy(node.Data.get(),data.data(),data.size());
    node.DataIndex=data.size();
    this->Send(node);
}
void Tcp_Connection::Send(const char* data,size_t dataLen){
    DataNode node(dataLen);
    memcpy(node.Data.get(),data,dataLen);
    node.DataIndex=dataLen;
    this->Send(node);
}

void Tcp_Connection::HandleSend(boost::system::error_code ec,std::shared_ptr<Tcp_Connection> sharedNode){
    if(ec){
        if(isStop.load()){
            return;
        }
        spdlog::error("Send Error,Error code is {},Error is {}",ec.value(),ec.message());
        Stop();
        return;
    }
    std::lock_guard<std::mutex> lk(SendMtx);
    SendQue.pop();
    if(SendQue.empty()){
        return;
    }
    auto& Fnode=SendQue.front();
    sock.async_send(boost::asio::buffer(Fnode.Data.get(),Fnode.DataIndex),
                    std::bind(&Tcp_Connection::HandleSend,this,std::placeholders::_1,shared_from_this()));
}
void Tcp_Connection::HandleRecv(boost::system::error_code ec,size_t RecvByteSize,std::shared_ptr<Tcp_Connection> sharedNode){
    if(ec){
        if(isStop.load()){
            return;
        }
        spdlog::error("Recv Error,Error code is {},Error is {}",ec.value(),ec.message());
        Stop();
        return;
    }
    Buf.DataIndex+=RecvByteSize;
    size_t byteUsed=Process(Buf);
    if(byteUsed==-1){
        spdlog::error("Process Data Error!");
        Buf.Clear();
    }else if(byteUsed<Buf.DataIndex){
        size_t remainSize=Buf.DataIndex-byteUsed;
        memmove(Buf.Data.get(),Buf.Data.get()+byteUsed,remainSize);
        Buf.DataIndex=remainSize;
    }else{
        Buf.Clear();
    }
    sock.async_read_some(boost::asio::buffer(Buf.Data.get()+Buf.DataIndex,Buf.DataSize-Buf.DataIndex),
                         std::bind(&Tcp_Connection::HandleRecv,this,std::placeholders::_1,std::placeholders::_2,shared_from_this()));
}

size_t Tcp_Connection::Process(DataNode &buf) {
    return 0;
}
