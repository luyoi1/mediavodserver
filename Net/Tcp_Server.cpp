//
// Created by luyoi on 2023/11/15.
//

#include "Tcp_Server.h"

Tcp_Server::Tcp_Server(boost::asio::io_context& ioc_,std::string ip,unsigned short port)
        :ioc(ioc_),ServerAccept(ioc,boost::asio::ip::tcp::endpoint(boost::asio::ip::make_address(ip),port))
        ,isStop(true){

}
Tcp_Server::~Tcp_Server(){
    this->Stop();
}

void Tcp_Server::Start(){
    isStop.store(false);
    auto conn=std::make_shared<Tcp_Connection>(ioc,shared_from_this());
    ServerAccept.async_accept(conn->GetSock(),std::bind(&Tcp_Server::HandleAccept,this,std::placeholders::_1,conn));
}
void Tcp_Server::Stop(){
    if(isStop.load()){
        return;
    }
    isStop.store(true);
    if(ServerAccept.is_open()){
        ServerAccept.cancel();
        ServerAccept.close();
    }
    ConnMap.clear();
}

void Tcp_Server::HandleAccept(boost::system::error_code ec,std::shared_ptr<Tcp_Connection> conn){
    if(ec){
        if(isStop.load()){
            return;
        }
        spdlog::error("Accept Error,Error code is {},Error is {}",ec.value(),ec.message());
        Stop();
        return;
    }
    AddConn(conn);
    conn->Start();
    auto newConn=std::make_shared<Tcp_Connection>(IOContextPool::GetInstance()->GetContext(),shared_from_this());
    ServerAccept.async_accept(newConn->GetSock(),std::bind(&Tcp_Server::HandleAccept,this,std::placeholders::_1,newConn));
}

void Tcp_Server::AddConn(std::shared_ptr <Tcp_Connection> node) {
    if(isStop.load()){
        return;
    }
    std::lock_guard<std::mutex> lk(ConnMapMtx);
    ConnMap[node->GetID()]=node;
}

void Tcp_Server::DelConn(size_t ID) {
    if(!ConnMap.count(ID)){
        return;
    }
    std::lock_guard<std::mutex> lk(ConnMapMtx);
    ConnMap.erase(ID);

}
