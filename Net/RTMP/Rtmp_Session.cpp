//
// Created by luyoi on 2023/11/15.
//

#include "Rtmp_Session.h"

#include <memory>
#include <utility>

#include "Rtmp_Connection.h"

namespace MS {
    Rtmp_Session::Rtmp_Session() {
    }

    Rtmp_Session::~Rtmp_Session() {
    }

    void Rtmp_Session::SetMetaData(AmfObjMap metaData) {
        std::lock_guard<std::mutex> lock(mtx);
        MetaData=std::move(metaData);
    }

    void Rtmp_Session::SetAvcSequenceHeader(std::shared_ptr<char> avcSequenceHeader, uint32_t avcSequenceHeaderSize) {
        std::lock_guard<std::mutex> lock(mtx);
        AvcSequenceHeader=std::move(avcSequenceHeader);
        AvcSequenceHeaderSize_=avcSequenceHeaderSize;
    }

    void Rtmp_Session::SetAacSequenceHeader(std::shared_ptr<char> aacSequenceHeader, uint32_t aacSequenceHeaderSize) {
        std::lock_guard<std::mutex> lock(mtx);
        AacSequenceHeader=std::move(aacSequenceHeader);
        AacSequenceHeaderSize_=aacSequenceHeaderSize;
    }

    void Rtmp_Session::AddConn(std::shared_ptr<Tcp_Connection> conn) {
        if(ConnMaps.contains(conn->GetID()))
            return;
        std::lock_guard<std::mutex> lock(mtx);
        ConnMaps[conn->GetID()]=conn;
        if(std::reinterpret_pointer_cast<Rtmp_Connection>(conn)->IsPublisher()) {
            AvcSequenceHeader=nullptr;
            AacSequenceHeader=nullptr;
            AvcSequenceHeaderSize_=0;
            AacSequenceHeaderSize_=0;
            GopCache.clear();
            GopIndex=0;
            HasPublishConn=true;
            Publisher=conn;
        }
        return;
    }

    void Rtmp_Session::RemoveConn(std::shared_ptr<Tcp_Connection> conn) {
        if(!ConnMaps.contains(conn->GetID()))
            return;
        std::lock_guard<std::mutex> lk(mtx);
        if(std::reinterpret_pointer_cast<Rtmp_Connection>(conn)->IsPublisher()) {
            AvcSequenceHeader=nullptr;
            AacSequenceHeader=nullptr;
            AvcSequenceHeaderSize_=0;
            AacSequenceHeaderSize_=0;
            GopCache.clear();
            GopIndex=0;
            HasPublishConn=false;
        }
        ConnMaps.erase(conn->GetID());
    }

    int Rtmp_Session::GetConnSize() {
        std::lock_guard<std::mutex> lk(mtx);
        return ConnMaps.size();
    }

    void Rtmp_Session::SendMetaData(AmfObjMap& metaData) {
        std::lock_guard<std::mutex> lk(mtx);
        std::vector<int> keyDel{};
        for(auto&&[key,val]:ConnMaps) {
            auto conn=std::reinterpret_pointer_cast<Rtmp_Connection>(val.lock());
            if(conn==nullptr) {
                keyDel.emplace_back(key);
                continue;
            }
            if(!conn->IsPlayer()) {
                continue;
            }
            conn->SendMetaData(MetaData);
        }
        for(auto data:keyDel) {
            if(ConnMaps.contains(data)) {
                ConnMaps.erase(data);
            }
        }
    }

    void Rtmp_Session::SendMediaData(uint8_t type, uint64_t timestamp, std::shared_ptr<char> data, uint32_t size) {
        std::lock_guard<std::mutex> lk(mtx);

        if(this->MaxGopCacheLen>0)
            SaveGop(type,timestamp,data,size);
        std::vector<int> keyDel{};
        for(auto&&[key,val]:ConnMaps) {
            auto conn=std::reinterpret_pointer_cast<Rtmp_Connection>(val.lock());
            if(conn==nullptr) {
                keyDel.emplace_back(key);
                continue;
            }
            if(!conn->IsPlayer()) {
                continue;
            }
            if(!conn->IsPlaying()) {
                conn->SendMetaData(MetaData);
                conn->SendMediaData(RTMP_AVC_SEQUENCE_HEADER,0,AvcSequenceHeader,AvcSequenceHeaderSize_);
                conn->SendMediaData(RTMP_AAC_SEQUENCE_HEADER,0,AacSequenceHeader,AacSequenceHeaderSize_);
                SendGop(conn);
            }
            conn->SendMediaData(type,timestamp,data,size);
        }
        for(auto data:keyDel) {
            if(ConnMaps.contains(data)) {
                ConnMaps.erase(data);
            }
        }
    }

    std::shared_ptr<Tcp_Connection> Rtmp_Session::GetPublisher() {
        std::lock_guard<std::mutex> lk(mtx);
        auto conn=Publisher.lock();
        if(conn) {
            return conn;
        }
        return {nullptr};
    }

    void Rtmp_Session::SetGopCache(uint32_t cacheLen) {
        std::lock_guard<std::mutex> lock(mtx);
        MaxGopCacheLen=cacheLen;
    }

    void Rtmp_Session::SaveGop(uint8_t type, uint64_t timestamp, std::shared_ptr<char> data, uint32_t size) {
        uint8_t* payload=(uint8_t*)data.get();
        uint8_t frameType=0;
        uint8_t codecID=0;
        std::shared_ptr<AVFrame> avframe=nullptr;
        std::shared_ptr<std::list<std::shared_ptr<AVFrame>>> gop=nullptr;
        if(GopCache.size()>0) {
            gop=GopCache[GopIndex];
        }

        if(type==RTMP_PACKET_TYPE_VIDEO) {
            frameType=(payload[0]>>4)&0x0F;
            codecID=payload[0]&0x0F;
            if(frameType==1&&codecID==RTMP_CODEC_ID_H264) {
                if(payload[1]==1) {//I帧
                    if(MaxGopCacheLen>0) {
                        if(GopCache.size()==2) {
                            GopCache.erase(GopCache.begin());
                        }
                        GopIndex+=1;
                        gop = std::make_shared<std::list<std::shared_ptr<AVFrame>>>();
                        GopCache[GopIndex]=gop;
                        avframe = std::make_shared<AVFrame>();
                    }
                }
            }else if(codecID==RTMP_CODEC_ID_H264&& gop!=nullptr) {
                if(MaxGopCacheLen>0&&!gop->empty()&&gop->size()<MaxGopCacheLen) {
                    avframe = std::make_shared<AVFrame>();
                }
            }
        }else if(type==RTMP_PACKET_TYPE_AUDIO&&gop!=nullptr) {
            uint8_t soundFormat=(payload[0]>>4)&0x0F;
            //uint8_t sound_size = (payload[0] >> 1) & 0x01;
            //uint8_t sound_rate = (payload[0] >> 2) & 0x03;

            if(soundFormat==RTMP_CODEC_ID_H264) {
                if(MaxGopCacheLen>0&&gop->size()>=2&&gop->size()<MaxGopCacheLen) {
                    if(timestamp>0) {
                        avframe = std::make_shared<AVFrame>();
                    }
                }
            }

            if(avframe!=nullptr&&gop!=nullptr) {
                avframe->type=type;
                avframe->timestamp=timestamp;
                avframe->size=size;
                avframe->data.reset(new char[size]);
                memcpy(avframe->data.get(),data.get(),size);
                gop->emplace_back(avframe);
            }
        }
    }

    void Rtmp_Session::SendGop(std::shared_ptr<Tcp_Connection> conn) {
        if(GopCache.empty()) {
            return;
        }
        auto gop=GopCache.begin()->second;
        for(auto&& iter:*gop) {
            if(iter->type==RTMP_PACKET_TYPE_VIDEO) {
                std::reinterpret_pointer_cast<Rtmp_Connection>(conn)->SendVideoData(iter->timestamp,iter->data,iter->size);
            }else if(iter->type==RTMP_PACKET_TYPE_AUDIO) {
                std::reinterpret_pointer_cast<Rtmp_Connection>(conn)->SendAudioData(iter->timestamp,iter->data,iter->size);
            }
        }
    }
} // MS