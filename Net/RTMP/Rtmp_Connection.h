//
// Created by luyoi on 2023/11/15.
//

#ifndef MEDIASIMPLESERVER_RTMP_CONNECTION_H
#define MEDIASIMPLESERVER_RTMP_CONNECTION_H

#include "../../Tool/Rtmp/RtmpChunk.h"
#include "../../Tool/Rtmp/RtmpHandShake.h"
#include "../../Tool/Rtmp/RtmpLinkState.h"
#include "Rtmp_Session.h"
#include "../Tcp_Server.h"
#include "../../Tool/Amf/AmfTool.h"

namespace MS {

    class Rtmp_Connection: public Tcp_Connection{
    public:
        enum class ConnectionState{
            HANDSHAKE,
            START_CONNECT,
            START_CREATE_STREAM,
            START_DELETE_STREAM,
            START_PLAY,
            START_PUBLISH
        };
        enum class ConnectionMode{
            RTMP_SERVER,
            RTMP_PUBLISHER,
            RTMP_CLIENT
        };

        Rtmp_Connection(boost::asio::io_context&ioc_,std::shared_ptr<Tcp_Server> server);

        const RtmpLinkState &getLinkState() const;
        const AmfObjMap &getMetaData() const;
        virtual bool IsPlayer();
        virtual bool IsPublisher();
        virtual bool IsPlaying();
        virtual bool IsPublishing();

        virtual bool SendMetaData(AmfObjMap metaData);
        virtual bool SendMediaData(uint8_t type, uint64_t timestamp, std::shared_ptr<char> payload, uint32_t payload_size);
        virtual bool SendVideoData(uint64_t timestamp, std::shared_ptr<char> payload, uint32_t payload_size);
        virtual bool SendAudioData(uint64_t timestamp, std::shared_ptr<char> payload, uint32_t payload_size);
    private:
        size_t Process(DataNode &buf) override;

        int HandleChunk(char* Buffer,uint32_t BufLen);
        void HandleMessage(RtmpMessage& rtmp_msg);
        void HandleInvoke(RtmpMessage& rtmp_msg);
        void HandleNotify(RtmpMessage& rtmp_msg);
        void HandleVideo(RtmpMessage& rtmp_msg);
        void HandleAudio(RtmpMessage& rtmp_msg);

        void HandleConnect();
        void HandleCreateStream();
        void HandlePublish();
        void HandlePlay();
        void HandlePlay2();
        void HandleDeleteStream();

        void SetPeerBandwidth();
        void SendAcknowledgement();
        void SetChunkSize();

        void SendInvokeMessage(uint32_t csid, std::shared_ptr<char> payload, uint32_t payload_size);
        void SendNotifyMessage(uint32_t csid, std::shared_ptr<char> payload, uint32_t payload_size);
        bool IsKeyFrame(std::shared_ptr<char> payload, uint32_t payload_size);
        void SendRtmpChunks(uint32_t csid, RtmpMessage& rtmp_msg);




        //TODO: server层与session层的实例指针
        std::weak_ptr<Rtmp_Session> RtmpSession;


        std::shared_ptr<RtmpHandShake> HandShakeInfo;
        std::shared_ptr<RtmpChunk> RtmpChunkInfo;
        ConnectionState ConnState;
        ConnectionMode ConnMode;

        size_t StreamID;
        RtmpLinkState LinkState;
        AmfObjMap MetaData;
        AmfDecoder amfDecode;
        AmfEncoder amfEncode;

        bool isPlaying = false;
        bool isPublishing = false;
        bool hasKeyFrame = false;
        std::shared_ptr<char> AvcSequenceHeader;
        std::shared_ptr<char> AacSequenceHeader;
        size_t AvcSequenceHeaderSize_ = 0;
        size_t AacSequenceHeaderSize_ = 0;
    };

} // MS

#endif //MEDIASIMPLESERVER_RTMP_CONNECTION_H
