//
// Created by luyoi on 2023/11/15.
//

#include "Rtmp_Connection.h"
#include "Rtmp_Server.h"

namespace MS {
    Rtmp_Connection::Rtmp_Connection(boost::asio::io_context &ioc_, std::shared_ptr<Tcp_Server> server)
    : Tcp_Connection(ioc_,server,8192),ConnState(ConnectionState::HANDSHAKE),ConnMode(ConnectionMode::RTMP_SERVER),
    HandShakeInfo(new RtmpHandShake(RtmpHandShake::State::HANDSHAKE_C0C1)),RtmpChunkInfo(new RtmpChunk),StreamID(0)
    {

    }

    const RtmpLinkState &Rtmp_Connection::getLinkState() const {
        return LinkState;
    }

    const AmfObjMap &Rtmp_Connection::getMetaData() const {
        return MetaData;
    }

    bool Rtmp_Connection::IsPlayer() {
        return ConnState==ConnectionState::START_PLAY;
    }

    bool Rtmp_Connection::IsPublisher() {
        return ConnState==ConnectionState::START_PUBLISH;
    }

    bool Rtmp_Connection::IsPlaying(){
        return isPlaying;
    }

    bool Rtmp_Connection::IsPublishing(){
        return isPublishing;
    }

    size_t Rtmp_Connection::Process(DataNode &buf) {
        int byteUsed=0;
        if(HandShakeInfo->isDone()){
            int ret= HandleChunk(buf.Data.get(),buf.DataIndex);
            if(ret<0){
                spdlog::error("Chunk Parse Error!");
                return Buf.DataIndex;
            }
            byteUsed+=ret;
        }else{//RTMP 握手
            std::shared_ptr<char> reply(new char[4096]);
            int res=HandShakeInfo->Parse(buf.Data.get(),buf.DataIndex,reply.get(),4096);

            if(res<0){
                spdlog::error("HandShake Parse Error");
                return -1;
            }
            if(res>0){
                this->Send(reply,res);
            }

            if(HandShakeInfo->isDone()){
                byteUsed+=1+1536+1536;
                if(buf.DataIndex>byteUsed){
                    byteUsed+= HandleChunk(buf.Data.get()+byteUsed,buf.DataIndex-byteUsed);
                }
            }else{
                byteUsed+=buf.DataIndex;
            }
        }
        return byteUsed;
    }

    int Rtmp_Connection::HandleChunk(char *Buffer, uint32_t BufLen) {
        int byteUsed=0;
        int remainLen=BufLen;
        do{
            RtmpMessage msg;
            int ret=RtmpChunkInfo->Parse(Buffer+byteUsed,BufLen-byteUsed,msg);
            if(ret>=0){
                if(msg.isReady()){
                    HandleMessage(msg);
                }
                if(ret==0){
                    break;
                }
                byteUsed+=ret;
            }else if(ret < 0){
                return -1;
            }
        }while(remainLen>byteUsed);
        return byteUsed;
    }

    void Rtmp_Connection::HandleMessage(RtmpMessage &rtmp_msg) {
        switch(rtmp_msg.TypeID)
        {
            case RTMP_PACKET_TYPE_VIDEO:
                HandleVideo(rtmp_msg);
                break;
            case RTMP_PACKET_TYPE_AUDIO:
                HandleAudio(rtmp_msg);
                break;
            case RTMP_PACKET_TYPE_INVOKE:
                HandleInvoke(rtmp_msg);
                break;
            case RTMP_PACKET_TYPE_INFO:
                HandleNotify(rtmp_msg);
                break;
            case RTMP_PACKET_TYPE_FLEX_MESSAGE:
                spdlog::warn("unsupported rtmp flex message");
                break;
            case RTMP_PACKET_TYPE_CHUNK_SIZE:{
                size_t ChunkSize=0;
                memcpy(&ChunkSize,rtmp_msg.Payload.get(),4);
                ChunkSize= ntohl(ChunkSize);
                RtmpChunkInfo->SetInChunkSize(ChunkSize);
                break;
            }
            case RTMP_PACKET_TYPE_CLIENT_BW:
                break;
            case RTMP_PACKET_TYPE_FLASH_VIDEO:
                spdlog::warn("unsupported rtmp flash video");
                break;
            case RTMP_PACKET_TYPE_BYTES_READ_REPORT:
                break;
            case RTMP_PACKET_TYPE_SERVER_BW:
                break;
            case RTMP_PACKET_TYPE_CONTROL:
                break;
            default:
                spdlog::warn("unkonw message type : {}", rtmp_msg.TypeID);
                break;
        }
        return;
    }

    void Rtmp_Connection::HandleInvoke(RtmpMessage &rtmp_msg) {
        char* ptr=rtmp_msg.Payload.get();
        int byteUsed=0;
        amfDecode.reset();
        int ret=amfDecode.decode(ptr,rtmp_msg.BodyLength,1);
        if(ret<0){
            spdlog::error("Invoke parse command error!");
            return;
        }
        byteUsed+=ret;
        std::string Method=amfDecode.getString();
        if(rtmp_msg.StreamID==0){
            byteUsed += amfDecode.decode(ptr+byteUsed, rtmp_msg.BodyLength-byteUsed);
            if(Method=="connect"){
                HandleConnect();
            }else if(Method=="createStream"){
                HandleCreateStream();
            }
        }else if(rtmp_msg.StreamID==StreamID){
            byteUsed += amfDecode.decode(ptr+byteUsed, rtmp_msg.BodyLength-byteUsed,3);
            LinkState.StreamName=amfDecode.getString();
            LinkState.StreamPath="/" + LinkState.App + "/" +LinkState.StreamName;

            if(rtmp_msg.BodyLength>byteUsed){
                byteUsed += amfDecode.decode(ptr+byteUsed, rtmp_msg.BodyLength-byteUsed);
            }

            if(Method == "publish") {
                HandlePublish();
            }
            else if(Method == "play") {
                HandlePlay();
            }
            else if(Method == "play2") {
                HandlePlay2();
            }
            else if(Method == "DeleteStream") {
                HandleDeleteStream();
            }
            else if (Method == "releaseStream") {

            }
        }
        return;
    }

    void Rtmp_Connection::HandleConnect() {
        if(!amfDecode.hasObject("app")) {
            spdlog::info("Connect Command error! App is empty!");
            return;
        }

        AmfObject amfObj = amfDecode.getObject("app");
        LinkState.App = amfObj.amf_string;
        if(LinkState.App.empty()) {
            spdlog::info("Connect Command error! App is empty!");
            return;
        }

        SendAcknowledgement();
        SetPeerBandwidth();
        SetChunkSize();

        AmfObjMap objects;
        amfEncode.reset();
        amfEncode.encodeString("_result", 7);
        amfEncode.encodeNumber(amfDecode.getNumber());

        objects["fmsVer"] = AmfObject(std::string("FMS/4,5,0,297"));
        objects["capabilities"] = AmfObject(255.0);
        objects["mode"] = AmfObject(1.0);
        amfEncode.encodeObjects(objects);
        objects.clear();
        objects["level"] = AmfObject(std::string("status"));
        objects["code"] = AmfObject(std::string("NetConnection.Connect.Success"));
        objects["description"] = AmfObject(std::string("Connection succeeded."));
        objects["objectEncoding"] = AmfObject(0.0);
        amfEncode.encodeObjects(objects);

        SendInvokeMessage(RTMP_CHUNK_INVOKE_ID, amfEncode.data(), amfEncode.size());
        return;
    }

    void Rtmp_Connection::HandleCreateStream() {
        StreamID=RtmpChunkInfo->GetStreamID();
        AmfObjMap objects;
        amfEncode.reset();
        amfEncode.encodeString("_result", 7);
        amfEncode.encodeNumber(amfDecode.getNumber());
        amfEncode.encodeObjects(objects);
        amfEncode.encodeNumber(StreamID);

        SendInvokeMessage(RTMP_CHUNK_INVOKE_ID, amfEncode.data(), amfEncode.size());

        return;
    }

    void Rtmp_Connection::SendInvokeMessage(uint32_t csid, std::shared_ptr<char> payload, uint32_t payload_size) {
        if(isStop.load()){
            return;
        }

        RtmpMessage msg;
        msg.TypeID=RTMP_PACKET_TYPE_INVOKE;
        msg.TimeStamp=0;
        msg.StreamID=StreamID;
        msg.Payload=payload;
        msg.BodyLength=payload_size;
        SendRtmpChunks(csid,msg);
    }

    void Rtmp_Connection::SendNotifyMessageA(uint32_t csid, std::shared_ptr<char> payload, uint32_t payload_size) {
        if(isStop.load()){
            return;
        }

        RtmpMessage msg;
        msg.TypeID=RTMP_PACKET_TYPE_INFO;
        msg.TimeStamp=0;
        msg.StreamID=StreamID;
        msg.Payload=payload;
        msg.BodyLength=payload_size;
        SendRtmpChunks(csid,msg);
    }

    bool Rtmp_Connection::IsKeyFrame(std::shared_ptr<char> payload, uint32_t payload_size) {
        unsigned char frame_type = (payload.get()[0] >> 4) & 0x0f;
        unsigned char codec_id = payload.get()[0] & 0x0f;
        return (frame_type == 1 && codec_id == RTMP_CODEC_ID_H264);
    }

    void Rtmp_Connection::SendRtmpChunks(uint32_t csid, RtmpMessage &rtmp_msg) {
        size_t capacity=rtmp_msg.BodyLength + rtmp_msg.BodyLength/ LinkState.MaxChunkSize *5 + 1024;
        std::shared_ptr<char> buffer(new char[capacity]);

        int size=RtmpChunkInfo->CreateChunk(csid,rtmp_msg,buffer.get(),capacity);
        if(size>0){
            this->Send(buffer,size);
        }
    }

    void Rtmp_Connection::SetPeerBandwidth() {
        std::shared_ptr<char> data(new char[5]);
        size_t pb= LinkState.getPeerBandWidth();
        pb=htonl(pb);
        memcpy(data.get(),&pb,4);
        data.get()[4] = 2;
        RtmpMessage rtmp_msg;
        rtmp_msg.TypeID = RTMP_PACKET_TYPE_CLIENT_BW;
        rtmp_msg.Payload = data;
        rtmp_msg.BodyLength = 5;
        SendRtmpChunks(RTMP_CHUNK_CONTROL_ID, rtmp_msg);
    }

    void Rtmp_Connection::SendAcknowledgement() {
        std::shared_ptr<char> data(new char[4]);
        size_t ack= LinkState.getAckNowLedgement();
        ack=htonl(ack);
        memcpy(data.get(),&ack,4);

        RtmpMessage rtmp_msg;
        rtmp_msg.TypeID = RTMP_PACKET_TYPE_SERVER_BW;
        rtmp_msg.Payload = data;
        rtmp_msg.BodyLength = 4;
        SendRtmpChunks(RTMP_CHUNK_CONTROL_ID, rtmp_msg);
    }

    void Rtmp_Connection::SetChunkSize() {
        RtmpChunkInfo->SetOutChunkSize(LinkState.getMaxChunkSize());
        std::shared_ptr<char> data(new char[4]);
        size_t ck= LinkState.getMaxChunkSize();
        ck=htonl(ck);
        memcpy(data.get(),&ck,4);
        RtmpMessage rtmp_msg;
        rtmp_msg.TypeID = RTMP_PACKET_TYPE_CHUNK_SIZE;
        rtmp_msg.Payload = data;
        rtmp_msg.BodyLength = 4;
        SendRtmpChunks(RTMP_CHUNK_CONTROL_ID, rtmp_msg);
    }

    void Rtmp_Connection::HandleNotify(RtmpMessage &rtmp_msg) {
        amfDecode.reset();
        int bytes_used = amfDecode.decode(rtmp_msg.Payload.get(), rtmp_msg.BodyLength, 1);
        if(bytes_used < 0) {
            return;
        }

        if(amfDecode.getString() == "@setDataFrame")
        {
            amfDecode.reset();
            int ret = amfDecode.decode(rtmp_msg.Payload.get()+bytes_used, rtmp_msg.BodyLength-bytes_used, 1);
            if(ret < 0) {
                spdlog::info("Notify Parse data error!");
                return;
            }
            bytes_used+=ret;

            if(amfDecode.getString() == "onMetaData") {
                ret = amfDecode.decode((const char *)rtmp_msg.Payload.get()+bytes_used, rtmp_msg.BodyLength-bytes_used);
                if(ret < 0) {
                    spdlog::info("Notify Parse data error!");
                    return;
                }
                MetaData = amfDecode.getObjects();

                auto server = std::reinterpret_pointer_cast<Rtmp_Server>(Server);
                if (!server) {
                    spdlog::info("Server is empty! error!");
                    return;
                }

                auto session = RtmpSession.lock();
                if(session) {
                    session->SetMetaData(MetaData);
                    session->SendMetaData(MetaData);
                }

            }
        }

        return;
    }

    void Rtmp_Connection::HandleVideo(RtmpMessage &rtmp_msg) {
        uint8_t type=RTMP_PACKET_TYPE_VIDEO;
        uint8_t *payload=(uint8_t*)rtmp_msg.Payload.get();
        uint32_t length=rtmp_msg.BodyLength;
        uint8_t FrameType = (payload[0]>>4)&0x0f;
        uint8_t CodecID=payload[0]&0x0f;

        auto server = std::reinterpret_pointer_cast<Rtmp_Server>(Server);
        if (!server) {
            spdlog::info("Server is empty! error!");
            return;
        }

        auto session = RtmpSession.lock();
        if (session == nullptr) {
            return;
        }
        if(FrameType==1&&CodecID==RTMP_CODEC_ID_H264){//关键帧
            if(payload[1]==0){//SPS PPS 等序列头
                AvcSequenceHeaderSize_=length;
                AvcSequenceHeader.reset(new char[length]);
                memcpy(AvcSequenceHeader.get(),payload,length);
                session->SetAvcSequenceHeader(AvcSequenceHeader, AvcSequenceHeaderSize_);
                type=RTMP_AVC_SEQUENCE_HEADER;
            }
        }
        session->SendMediaData(type, rtmp_msg.GetTimeStamp(), rtmp_msg.Payload, rtmp_msg.BodyLength);//发送时间戳时注意属性
    }

    void Rtmp_Connection::HandleAudio(RtmpMessage &rtmp_msg) {
        uint8_t type = RTMP_PACKET_TYPE_AUDIO;
        uint8_t *payload = (uint8_t *)rtmp_msg.Payload.get();
        uint32_t length = rtmp_msg.BodyLength;
        uint8_t sound_format = (payload[0] >> 4) & 0x0f;
        //uint8_t sound_size = (payload[0] >> 1) & 0x01;
        //uint8_t sound_rate = (payload[0] >> 2) & 0x03;
        uint8_t codec_id = payload[0] & 0x0f;

        auto server = std::reinterpret_pointer_cast<Rtmp_Server>(Server);
        if (!server) {
            spdlog::info("Server is empty! error!");
            return;
        }

        auto session = RtmpSession.lock();
        if (session == nullptr) {
            return;
        }

        if (sound_format == RTMP_CODEC_ID_AAC && payload[1] == 0) {//头数据
            AacSequenceHeaderSize_ = length;
            AacSequenceHeader.reset(new char[length], std::default_delete<char[]>());
            memcpy(AacSequenceHeader.get(), payload, length);
            session->SetAacSequenceHeader(AacSequenceHeader, AacSequenceHeaderSize_);
            type = RTMP_AAC_SEQUENCE_HEADER;
        }
        session->SendMediaData(type, rtmp_msg.GetTimeStamp(), rtmp_msg.Payload, rtmp_msg.BodyLength);
    }

    void Rtmp_Connection::HandlePublish() {
        AmfObjMap objects;
        amfEncode.reset();
        amfEncode.encodeString("onStatus", 8);
        amfEncode.encodeNumber(0);
        amfEncode.encodeObjects(objects);

        bool is_error = false;

        if(std::reinterpret_pointer_cast<Rtmp_Server>(Server)->HasPublisher(LinkState.getStreamPath())) {
            is_error = true;
            objects["level"] = AmfObject(std::string("error"));
            objects["code"] = AmfObject(std::string("NetStream.Publish.BadName"));
            objects["description"] = AmfObject(std::string("Stream already publishing."));
        }
        else if(ConnState == ConnectionState::START_PUBLISH) {
            is_error = true;
            objects["level"] = AmfObject(std::string("error"));
            objects["code"] = AmfObject(std::string("NetStream.Publish.BadConnection"));
            objects["description"] = AmfObject(std::string("Connection already publishing."));
        }
        /* else if(0)  {
            // 认证处理
        } */
        else {
            objects["level"] = AmfObject(std::string("status"));
            objects["code"] = AmfObject(std::string("NetStream.Publish.Start"));
            objects["description"] = AmfObject(std::string("Start publising."));
            std::reinterpret_pointer_cast<Rtmp_Server>(Server)->AddSession(LinkState.getStreamPath());
            RtmpSession = std::reinterpret_pointer_cast<Rtmp_Server>(Server)->GetSession(LinkState.getStreamPath());

            if (Server) {
                //server->NotifyEvent("publish.start", stream_path_);
                //打印信息用
            }
        }

        amfEncode.encodeObjects(objects);
        SendInvokeMessage(RTMP_CHUNK_INVOKE_ID, amfEncode.data(), amfEncode.size());

        if(is_error) {
            // Close ?
            spdlog::error("PublishConnecion Starting!");
            return;
        }
        else {
            ConnState = ConnectionState::START_PUBLISH;
            isPublishing = true;
        }

        auto session = RtmpSession.lock();
        if(session) {
            session->SetGopCache(LinkState.getMaxGopCacheLen());
            session->AddConn(shared_from_this());
        }

        return;
    }

    void Rtmp_Connection::HandlePlay() {

        AmfObjMap objects;
        amfEncode.reset();
        amfEncode.encodeString("onStatus", 8);
        amfEncode.encodeNumber(0);
        amfEncode.encodeObjects(objects);
        objects["level"] = AmfObject(std::string("status"));
        objects["code"] = AmfObject(std::string("NetStream.Play.Reset"));
        objects["description"] = AmfObject(std::string("Resetting and playing stream."));
        amfEncode.encodeObjects(objects);
        SendInvokeMessage(RTMP_CHUNK_INVOKE_ID, amfEncode.data(), amfEncode.size());

        objects.clear();
        amfEncode.reset();
        amfEncode.encodeString("onStatus", 8);
        amfEncode.encodeNumber(0);
        amfEncode.encodeObjects(objects);
        objects["level"] = AmfObject(std::string("status"));
        objects["code"] = AmfObject(std::string("NetStream.Play.Start"));
        objects["description"] = AmfObject(std::string("Started playing."));
        amfEncode.encodeObjects(objects);
        SendInvokeMessage(RTMP_CHUNK_INVOKE_ID, amfEncode.data(), amfEncode.size());


        amfEncode.reset();
        amfEncode.encodeString("|RtmpSampleAccess", 17);
        amfEncode.encodeBoolean(true);
        amfEncode.encodeBoolean(true);
        this->SendNotifyMessage(RTMP_CHUNK_DATA_ID, amfEncode.data(), amfEncode.size());

        ConnState = ConnectionState::START_PLAY;

        RtmpSession = std::reinterpret_pointer_cast<Rtmp_Server>(Server)->GetSession(LinkState.getStreamPath());
        auto session = RtmpSession.lock();
        if(session) {
            session->AddConn(shared_from_this());
        }
        return;
    }

    void Rtmp_Connection::HandlePlay2() {
        HandlePlay();
    }

    void Rtmp_Connection::HandleDeleteStream() {
        if(LinkState.getStreamPath().empty()) {
            auto session = RtmpSession.lock();
            if(session) {
                session->RemoveConn(shared_from_this());
            }
            isPlaying = false;
            isPublishing = false;
            hasKeyFrame=false;
            RtmpChunkInfo->Clear();
        }
    }

    bool Rtmp_Connection::SendMetaData(AmfObjMap metaData) {
        if(isStop.load())
            return false;

        if(metaData.size()==0)
            return false;
        amfEncode.reset();
        amfEncode.encodeString("onMetaData",10);
        amfEncode.encodeECMA(metaData);

        SendNotifyMessageA(RTMP_CHUNK_DATA_ID,amfEncode.data(),amfEncode.size());

        return true;
    }

    bool Rtmp_Connection::SendMediaData(uint8_t type, uint64_t timestamp, std::shared_ptr<char> payload,uint32_t payload_size) {
        if(isStop.load())
            return false;
        if(payload_size==0)
            return false;

        isPlaying=true;
        if(type==RTMP_AVC_SEQUENCE_HEADER) {
            AvcSequenceHeader=payload;
            AvcSequenceHeaderSize_=payload_size;
        }else if(type==RTMP_AAC_SEQUENCE_HEADER) {
            AacSequenceHeader=payload;
            AacSequenceHeaderSize_=payload_size;
        }

        auto conn=std::dynamic_pointer_cast<Rtmp_Connection>(shared_from_this());
        if (!conn->hasKeyFrame && conn->AvcSequenceHeaderSize_ > 0&& (type != RTMP_AVC_SEQUENCE_HEADER)&& (type != RTMP_AAC_SEQUENCE_HEADER)) {
            if (conn->IsKeyFrame(payload, payload_size)) {
                conn->hasKeyFrame = true;
            }
            else {
                return false;
            }
        }

        RtmpMessage rtmp_msg;
        rtmp_msg.TimeStamp = timestamp;
        rtmp_msg.StreamID = conn->StreamID;
        rtmp_msg.Payload = payload;
        rtmp_msg.BodyLength = payload_size;

        if (type == RTMP_PACKET_TYPE_VIDEO || type == RTMP_AVC_SEQUENCE_HEADER) {
            rtmp_msg.TypeID = RTMP_PACKET_TYPE_VIDEO;
            conn->SendRtmpChunks(RTMP_CHUNK_VIDEO_ID, rtmp_msg);
        }
        else if (type == RTMP_PACKET_TYPE_AUDIO || type == RTMP_AAC_SEQUENCE_HEADER) {
            rtmp_msg.TypeID = RTMP_PACKET_TYPE_AUDIO;
            conn->SendRtmpChunks(RTMP_CHUNK_AUDIO_ID, rtmp_msg);
        }

        return true;
    }

    bool Rtmp_Connection::SendVideoData(uint64_t timestamp, std::shared_ptr<char> payload, uint32_t payload_size) {
        if(payload_size==0)
            return false;

        auto conn=std::dynamic_pointer_cast<Rtmp_Connection>(shared_from_this());
        RtmpMessage rtmp_msg;
        rtmp_msg.TypeID = RTMP_PACKET_TYPE_VIDEO;
        rtmp_msg.TimeStamp = timestamp;
        rtmp_msg.StreamID = conn->StreamID;
        rtmp_msg.Payload = payload;
        rtmp_msg.BodyLength = payload_size;
        conn->SendRtmpChunks(RTMP_CHUNK_VIDEO_ID,rtmp_msg);
        return true;
    }

    bool Rtmp_Connection::SendAudioData(uint64_t timestamp, std::shared_ptr<char> payload, uint32_t payload_size) {
        if(payload_size==0)
            return false;

        auto conn=std::dynamic_pointer_cast<Rtmp_Connection>(shared_from_this());
        RtmpMessage rtmp_msg;
        rtmp_msg.TypeID = RTMP_PACKET_TYPE_AUDIO;
        rtmp_msg.TimeStamp = timestamp;
        rtmp_msg.StreamID = conn->StreamID;
        rtmp_msg.Payload = payload;
        rtmp_msg.BodyLength = payload_size;
        conn->SendRtmpChunks(RTMP_CHUNK_AUDIO_ID,rtmp_msg);
        return true;
    }


} // MS