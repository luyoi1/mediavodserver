//
// Created by luyoi on 2023/11/15.
//

#ifndef MEDIASIMPLESERVER_RTMP_SERVER_H
#define MEDIASIMPLESERVER_RTMP_SERVER_H

#include "Rtmp_Session.h"
#include "../Tcp_Server.h"

namespace MS {

    class Rtmp_Server: public Tcp_Server{
    public:
        Rtmp_Server(boost::asio::io_context& ioc,std::string ip,uint16_t port);

        void AddSession(std::string_view Name,std::shared_ptr<Rtmp_Session> session);
        void AddSession(std::string_view Name);
        void RemoveSession(std::string_view Name);
        std::shared_ptr<Rtmp_Session> GetSession(std::string_view Name);
        bool HasSession(std::string_view Name);
        bool HasPublisher(std::string_view Name);

        void Start() override;

    protected:
        void HandleAccept(boost::system::error_code ec, std::shared_ptr<Tcp_Connection> conn) override;

    private:


        std::mutex MapMtx;
        std::map<std::string,std::shared_ptr<Rtmp_Session>> SessionMap;
    };

} // MS

#endif //MEDIASIMPLESERVER_RTMP_SERVER_H
