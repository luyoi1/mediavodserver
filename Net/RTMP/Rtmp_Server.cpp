//
// Created by luyoi on 2023/11/15.
//

#include "Rtmp_Server.h"

#include "Rtmp_Connection.h"

namespace MS {
    Rtmp_Server::Rtmp_Server(boost::asio::io_context& ioc, std::string ip, uint16_t port):
        Tcp_Server(ioc,ip,port)
    {
    }

    void Rtmp_Server::AddSession(std::string_view Name, std::shared_ptr<Rtmp_Session> session) {
        if(SessionMap.contains(Name.data()))
            return;
        std::lock_guard<std::mutex> lk(MapMtx);
        SessionMap[Name.data()]=std::move(session);
    }

    void Rtmp_Server::AddSession(std::string_view Name) {
        if(SessionMap.contains(Name.data()))
            return;
        std::lock_guard<std::mutex> lk(MapMtx);
        SessionMap[Name.data()]=std::make_shared<Rtmp_Session>();
    }

    void Rtmp_Server::RemoveSession(std::string_view Name) {
        if(!SessionMap.contains(Name.data()))
            return;
        std::lock_guard<std::mutex> lk(MapMtx);
        SessionMap.erase(Name.data());
    }

    std::shared_ptr<Rtmp_Session> Rtmp_Server::GetSession(std::string_view Name) {
        if(!SessionMap.contains(Name.data()))
            return {nullptr};
        return SessionMap[Name.data()];
    }

    bool Rtmp_Server::HasSession(std::string_view Name) {
        return SessionMap.contains(Name.data());
    }

    bool Rtmp_Server::HasPublisher(std::string_view Name) {
        auto session =GetSession(Name.data());
        if(session==nullptr) {
            return false;
        }
        return (session->GetPublisher()!=nullptr);
    }

    void Rtmp_Server::Start() {
        isStop.store(false);
        auto conn=std::make_shared<Rtmp_Connection>(ioc,shared_from_this());
        ServerAccept.async_accept(conn->GetSock(),std::bind(&Rtmp_Server::HandleAccept,this,std::placeholders::_1,conn));
    }

    void Rtmp_Server::HandleAccept(boost::system::error_code ec, std::shared_ptr<Tcp_Connection> conn) {
        if(ec){
            if(isStop.load()){
                return;
            }
            spdlog::error("Accept Error,Error code is {},Error is {}",ec.value(),ec.message());
            Stop();
            return;
        }
        AddConn(conn);
        std::reinterpret_pointer_cast<Rtmp_Connection>(conn)->Start();
        auto newConn=std::make_shared<Rtmp_Connection>(IOContextPool::GetInstance()->GetContext(),shared_from_this());
        ServerAccept.async_accept(newConn->GetSock(),std::bind(&Rtmp_Server::HandleAccept,this,std::placeholders::_1,newConn));
    }
} // MS