//
// Created by luyoi on 2023/11/15.
//

#ifndef MEDIASIMPLESERVER_RTMP_SESSION_H
#define MEDIASIMPLESERVER_RTMP_SESSION_H

#include "../../Tool/Rtmp/RtmpSink.h"
#include "../Tcp_Connection.h"

namespace MS {
    struct AVFrame {
        uint8_t type=0;
        uint64_t timestamp=0;
        uint32_t size=0;
        std::shared_ptr<char> data=nullptr;
    };


    class Rtmp_Session {
    public:
        Rtmp_Session();
        virtual ~Rtmp_Session();

        void SetMetaData(AmfObjMap metaData);
        void SetAvcSequenceHeader(std::shared_ptr<char> avcSequenceHeader,uint32_t avcSequenceHeaderSize);
        void SetAacSequenceHeader(std::shared_ptr<char> aacSequenceHeader,uint32_t aacSequenceHeaderSize);

        void AddConn(std::shared_ptr<Tcp_Connection> conn);
        void RemoveConn(std::shared_ptr<Tcp_Connection> conn);
        int GetConnSize();

        void SendMetaData(AmfObjMap& metaData);
        void SendMediaData(uint8_t type,uint64_t timestamp,std::shared_ptr<char> data,uint32_t size);

        std::shared_ptr<Tcp_Connection> GetPublisher();
        void SetGopCache(uint32_t cacheLen);
        void SaveGop(uint8_t type,uint64_t timestamp,std::shared_ptr<char> data,uint32_t size);
    private:
        void SendGop(std::shared_ptr<Tcp_Connection> conn);

        std::mutex mtx;
        AmfObjMap MetaData;
        bool HasPublishConn=false;
        std::weak_ptr<Tcp_Connection> Publisher;
        std::map<uint32_t,std::weak_ptr<Tcp_Connection>> ConnMaps;

        std::shared_ptr<char> AvcSequenceHeader;
        std::shared_ptr<char> AacSequenceHeader;
        size_t AvcSequenceHeaderSize_ = 0;
        size_t AacSequenceHeaderSize_ = 0;
        uint64_t GopIndex=0;
        uint32_t MaxGopCacheLen=0;

        std::map<uint64_t,std::shared_ptr<std::list<std::shared_ptr<AVFrame>>>> GopCache;
    };

} // MS

#endif //MEDIASIMPLESERVER_RTMP_SESSION_H
