//
// Created by luyoi on 2023/11/15.
//

#ifndef MEDIASIMPLESERVER_TCP_SERVER_H
#define MEDIASIMPLESERVER_TCP_SERVER_H


#pragma once

#include "../Tool/IOContextPool.h"
#include "Tcp_Connection.h"
#include <boost/asio.hpp>
#include <cstddef>
#include <memory>
#include <map>

class Tcp_Server:public std::enable_shared_from_this<Tcp_Server>{
public:
    Tcp_Server(boost::asio::io_context& ioc_,std::string ip,unsigned short port);
    virtual ~Tcp_Server();

    virtual void Start();
    virtual void Stop();
    void AddConn(std::shared_ptr<Tcp_Connection> node);
    void DelConn(size_t ID);
protected:

    virtual void HandleAccept(boost::system::error_code ec,std::shared_ptr<Tcp_Connection> conn);

    boost::asio::io_context& ioc;//用Ioc池代替
    boost::asio::ip::tcp::acceptor ServerAccept;
    std::mutex ConnMapMtx;
    std::map<size_t,std::shared_ptr<Tcp_Connection>> ConnMap;
    std::atomic_bool isStop;
};


#endif //MEDIASIMPLESERVER_TCP_SERVER_H
