//
// Created by luyoi on 2023/11/15.
//

#ifndef MEDIASIMPLESERVER_TCP_CONNECTION_H
#define MEDIASIMPLESERVER_TCP_CONNECTION_H

#pragma once
#include <atomic>
#include <cstddef>
#include <memory>
#include <mutex>
#include <spdlog/spdlog.h>
#include <boost/asio.hpp>
#include <queue>
#include <random>

struct DataNode{
    DataNode(size_t size);
    DataNode(std::shared_ptr<char> data,size_t size);

    void Clear();
    void ReSize(size_t size);

    std::shared_ptr<char> Data;
    size_t DataSize;
    size_t DataIndex;
};

class Tcp_Server;
class Tcp_Connection:public std::enable_shared_from_this<Tcp_Connection>{
public:
    Tcp_Connection(boost::asio::io_context& ioc_,std::shared_ptr<Tcp_Server> server,size_t BufSize=4096);
    virtual ~Tcp_Connection();
    boost::asio::ip::tcp::socket& GetSock();
    size_t GetID()const;
    virtual void Start();
    virtual void Stop();
    void Send(DataNode& node);
    void Send(std::shared_ptr<char> data,size_t dataLen);
    void Send(std::string data);
    void Send(const char* data,size_t dataLen);
protected:
    virtual size_t Process(DataNode& buf);
    virtual void HandleSend(boost::system::error_code ec,std::shared_ptr<Tcp_Connection> sharedNode);
    virtual void HandleRecv(boost::system::error_code ec,size_t RecvByteSize,std::shared_ptr<Tcp_Connection> sharedNode);

    boost::asio::io_context& ioc;
    boost::asio::ip::tcp::socket sock;
    std::queue<DataNode> SendQue;
    std::mutex SendMtx;
    DataNode Buf;
    std::atomic_bool isStop;
    std::shared_ptr<Tcp_Server> Server;
    size_t ConnID;
};


#endif //MEDIASIMPLESERVER_TCP_CONNECTION_H
