//
// Created by luyoi on 2023/10/16.
//

#include "Rtsp_Server.h"

Rtsp_Server::Rtsp_Server(std::string ip, unsigned short RtspPort,unsigned short RtpUdpPort)
    : Tcp_Server(IOContextPool::GetInstance()->GetContext(),ip,RtspPort),
    UdpServer(std::make_shared<Rtsp_Udp_Server>(RtpUdpPort,RecvUdpQue,RecvMutex))
{

}

Rtsp_Server::~Rtsp_Server() {
    Stop();
    UdpServer.reset();
}

void Rtsp_Server::Start() {
    isStop.store(false);
    auto conn=std::make_shared<Rtsp_Connection>(IOContextPool::GetInstance()->GetContext(),shared_from_this());
    ServerAccept.async_accept(conn->GetSock(),
                              std::bind(&Rtsp_Server::HandleAccept,this,std::placeholders::_1,conn));
}

void Rtsp_Server::HandleAccept(boost::system::error_code ec, std::shared_ptr<Tcp_Connection> conn) {
    if(ec){
        spdlog::error("RtspServer Accept Error,Error is {}",ec.what());
        return;
    }
    AddConn(conn);
    conn->Start();
    auto newConn=std::make_shared<Rtsp_Connection>(IOContextPool::GetInstance()->GetContext(),shared_from_this());
    ServerAccept.async_accept(newConn->GetSock(),std::bind(&Rtsp_Server::HandleAccept,this,std::placeholders::_1,newConn));
}

void Rtsp_Server::AddSession(std::string_view Name, const std::shared_ptr<Session>& session) {
    std::lock_guard<std::mutex> lk(SessionMtx);
    SessionMap[std::string(Name)]=session;
}


