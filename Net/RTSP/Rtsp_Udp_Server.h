//
// Created by luyoi on 2023/10/21.
//

#ifndef MEDIASIMPLESERVER_RTSP_UDP_SERVER_H
#define MEDIASIMPLESERVER_RTSP_UDP_SERVER_H

#include<boost/asio.hpp>
#include<mutex>
#include<condition_variable>
#include<queue>
#include<spdlog/spdlog.h>
#include"../../Tool/IOContextPool.h"
#include"../../constant/Const.h"


class Rtsp_Udp_Server: public std::enable_shared_from_this<Rtsp_Udp_Server>{
public:
    Rtsp_Udp_Server(unsigned short RtpPort,std::queue<std::shared_ptr<UdpNode>>& Output,std::mutex& OutMutex);
    ~Rtsp_Udp_Server();
    void PushNode(std::shared_ptr<UdpNode> node);
    void Start();
    void SendTo(const char* data,size_t dataSize,unsigned port,std::string_view ip);
private:
    void PushRtpNode(std::shared_ptr<UdpNode> node);
    void PushRtcpNode(std::shared_ptr<UdpNode> node);
    void RecvRtpHandler(boost::system::error_code ec,size_t byteSize);
    void RecvRtcpHandler(boost::system::error_code ec,size_t byteSize);
    void SendHandler(boost::system::error_code ec,SendType type);
    void PushOutputQue(std::shared_ptr<UdpNode> node);

    std::queue<std::shared_ptr<UdpNode>> SendRtpQueue;
    std::queue<std::shared_ptr<UdpNode>> SendRtcpQueue;
    std::mutex SendRtpMutex;
    std::mutex SendRtcpMutex;
    boost::asio::ip::udp::socket RtpSock;
    boost::asio::ip::udp::socket RtcpSock;
    std::shared_ptr<unsigned char> RtpBuffer;
    std::shared_ptr<unsigned char> RtcpBuffer;

    std::queue<std::shared_ptr<UdpNode>>& OutputQueue;
    std::mutex& OutputMutex;
};


#endif //MEDIASIMPLESERVER_RTSP_UDP_SERVER_H
