//
// Created by luyoi on 2023/10/16.
//

#include "Rtsp_Connection.h"
#include "Rtsp_Server.h"
#include <utility>
#include "Rtsp_Tool.h"
#include "Rtsp_Server.h"

Rtsp_Connection::Rtsp_Connection(boost::asio::io_context &ioc_,std::shared_ptr<Tcp_Server> Main):
        Tcp_Connection(ioc_,Main,Recv_Buffer_Len)

{
    std::mt19937 engine(std::random_device{}());
    std::uniform_int_distribution<unsigned int> distribution;
    ConnID = distribution(engine);

    //初始化
    Init();
}

void Rtsp_Connection::Init(){
    std::vector<std::pair<RtspMethod,std::function<void(std::map<std::string,std::string>&)>>> FuncVec{
            {RtspMethod::OPTIONS,std::bind(&Rtsp_Connection::OptionHandler,this,std::placeholders::_1)},
            {RtspMethod::DESCRIBE,std::bind(&Rtsp_Connection::DescribeHandler,this,std::placeholders::_1)},
            {RtspMethod::SETUP,std::bind(&Rtsp_Connection::SetupHandler,this,std::placeholders::_1)},
            {RtspMethod::PLAY,std::bind(&Rtsp_Connection::PlayHandler,this,std::placeholders::_1)},
            {RtspMethod::TEARDOWN,std::bind(&Rtsp_Connection::TeardownHandler,this,std::placeholders::_1)},
    };
    for(auto& data:FuncVec){
        MethodFunc.emplace(data);
    }
}

Rtsp_Connection::~Rtsp_Connection() {
    this->Teardown();
    Stop();
}


size_t Rtsp_Connection::Process(DataNode &buf) {
    int remainLcSize=buf.DataIndex;//缓冲区数据总长
    //TODO 解析Rtsp请求，对请求做出相应动作与回应，未使用的数据存储在原缓冲区中
    std::string str{};
    str.resize(remainLcSize);
    memcpy(str.data(),buf.Data.get(),remainLcSize);
    auto index=str.find("\r\n\r\n");
    if(index==std::string::npos){
        spdlog::error("req parse Error!");
        return 0;
    }
    auto ReqStr=str.substr(0,index);//完整请求
    std::map<std::string,std::string> fields;
    auto method=ParseRtspSeq(ReqStr,fields);
    //TODO:解析请求，获取相应字段
    MethodFunc[method](fields);
    return str.size()+sizeof("\r\n\r\n");
}


void Rtsp_Connection::SendRtpPacket(std::shared_ptr<RtpPacket> node, unsigned short Channel, unsigned short port,std::shared_ptr<Tcp_Connection> thisNode) {
    if(this->isStop){
        return;
    }
    if(type==ConnType::OverTcp){
        node->OverTcpHeaderPointer[0]='$';
        node->OverTcpHeaderPointer[1]=Channel;
        node->OverTcpHeaderPointer[2]=(unsigned char)(((node->dataLength-4)&0xFF00)>>8);
        node->OverTcpHeaderPointer[3]=(unsigned char)(((node->dataLength-4)&0xFF));
        auto datanode=DataNode(std::reinterpret_pointer_cast<char>(node->Buffer),node->dataLength);
        this->Send(datanode);
    }else if(type==ConnType::OverUdp){
        std::dynamic_pointer_cast<Rtsp_Server>(Server)->UdpServer->SendTo((char*)node->OverUdpHeaderPointer,node->dataLength-4,port,sock.remote_endpoint().address().to_string());
    }
    return;
}

void Rtsp_Connection::SendUdpPacket(std::shared_ptr<UdpNode>& node,unsigned short port) {
    node->Port=port;
    std::dynamic_pointer_cast<Rtsp_Server>(Server)->UdpServer->PushNode(node);
}

void Rtsp_Connection::SendTcpPacket(DataNode& node) {
    std::unique_lock<std::mutex> uql(SendMtx);
    SendQue.push(node);
    if(SendQue.size()>1){
        return;
    }
    auto FrontNode=SendQue.front();
    sock.async_send(boost::asio::buffer(FrontNode.Data.get(), FrontNode.DataIndex),
                    std::bind(&Rtsp_Connection::HandleSend,this,std::placeholders::_1,shared_from_this()));
}

void Rtsp_Connection::Play() {
    for(auto&&[key,val]:StreamLinkMap){
        val->RunTimer();//开启定时任务
    }
}

void Rtsp_Connection::OptionHandler(std::map<std::string,std::string>& maps) {
    auto resp=fmt::format("RTSP/1.0 200 OK\r\n"
                          "CSeq: {}\r\n"
                          "Public: DESCRIBE, ANNOUNCE, SETUP, PLAY, RECORD, PAUSE,TEARDOWN\r\n"//GET_PARAMETER,
                          "Server: {}\r\n"
                          "\r\n",maps["CSeq"],SERVER_VERSION);
    std::shared_ptr<char> node=std::shared_ptr<char>(new char[resp.size()]);
    memcpy(node.get(),resp.data(),resp.size());
    auto datanode=DataNode(node,resp.size());
    Send(datanode);
}

void Rtsp_Connection::DescribeHandler(std::map<std::string,std::string>& maps) {
    auto url=maps["Url"];
    auto off=url.find_last_of('/');
    LivePath=url.substr(off+1);

    if(!std::dynamic_pointer_cast<Rtsp_Server>(Server)->SessionMap.count(LivePath)){
        return;
    }
    auto session=std::dynamic_pointer_cast<Rtsp_Server>(Server)->SessionMap[LivePath];
    session->AddConn(GetID());
    auto GopMap=session->GetGopMap();
    for(auto&&[key,val]:GopMap){
        StreamLinkMap[key]=std::make_shared<StreamLinkState>(IOContextPool::GetInstance()->GetContext(),val,key);
        StreamLinkMap[key]->SetSendFun(std::bind(&Rtsp_Connection::SendRtpPacket,this,std::placeholders::_1,std::placeholders::_2,std::placeholders::_3,shared_from_this()));
        val->GetSink()->RunTimer();
    }

    auto sdp=generateSDPDescription();
    auto resp=fmt::format("RTSP/1.0 200 OK\r\n"
                          "CSeq: {}\r\n"
                          "Content-Length: {}\r\n"
                          "Content-Type: application/sdp\r\n"
                          "\r\n"
                          "{}",maps["CSeq"],sdp.size(),sdp);
    std::shared_ptr<char> node=std::shared_ptr<char>(new char[resp.size()]);
    memcpy(node.get(),resp.data(),resp.size());
    auto datanode=DataNode(node,resp.size());
    Send(datanode);
}

void Rtsp_Connection::SetupHandler(std::map<std::string,std::string>& maps) {
    std::string resp{};
    auto url=maps["Url"];
    auto index=0;
    char lcStr[10]{};
    auto off=url.find("track");
    if(off==std::string::npos){
        return;
    }
    index=std::stoi(url.substr(off+5));
    auto transport=maps["Transport"];
    if(transport.find("RTP/AVP/TCP")!=std::string::npos){
        type=ConnType::OverTcp;
        int rtpChannel=0,rtcpChannel=0;
        sscanf_s(transport.c_str(),"RTP/AVP/TCP;unicast;interleaved=%d-%d",&rtpChannel,&rtcpChannel);
        StreamLinkMap[index]->SetChannel(rtpChannel);
    }else{
        type=ConnType::OverUdp;
        int rtpPort=0,rtcpPort=0;
        sscanf_s(transport.c_str(),"RTP/AVP/UDP;unicast;client_port=%d-%d",&rtpPort,&rtcpPort);
        StreamLinkMap[index]->SetPort(rtpPort);
    }




    if(type==ConnType::OverTcp){
        resp=fmt::format("RTSP/1.0 200 OK\r\n"
                         "CSeq: {}\r\n"
                         "Server: {}\r\n"
                         "Transport: RTP/AVP/TCP;unicast;interleaved={}-{}\r\n"
                         "Session: {:08x}\r\n"
                         "\r\n",maps["CSeq"],SERVER_VERSION,
                         StreamLinkMap[index]->getRtpChannel(),StreamLinkMap[index]->getRtcpChannel(),ConnID);
    }else if(type==ConnType::OverUdp){
        resp=fmt::format("RTSP/1.0 200 OK\r\n"
                         "CSeq: {}\r\n"
                         "Server: {}\r\n"
                         "Transport: RTP/AVP;unicast;client_port={}-{};server_port={}-{}\r\n"
                         "Session: {:08x}\r\n"
                         "\r\n",
                         maps["CSeq"],SERVER_VERSION,
                         StreamLinkMap[index]->getRemoteLocalRtpPort(),
                         StreamLinkMap[index]->getRemoteLocalRtcpPort(),
                         UdpListenPort,
                         UdpListenPort+1,
                         ConnID);
    }
    std::shared_ptr<char> node=std::shared_ptr<char>(new char[resp.size()]);
    memcpy(node.get(),resp.data(),resp.size());
    auto datanode=DataNode(node,resp.size());
    Send(datanode);
}

void Rtsp_Connection::PlayHandler(std::map<std::string,std::string>& maps) {
    auto resp=fmt::format("RTSP/1.0 200 OK\r\n"
                          "CSeq: {}\r\n"
                          "Server: {}\r\n"
                          "Range: npt=0.000-\r\n"
                          "Session: {}; timeout=60\r\n"
                          "\r\n",
                          maps["CSeq"], SERVER_VERSION,
                          ConnID);
    std::shared_ptr<char> node=std::shared_ptr<char>(new char[resp.size()]);
    memcpy(node.get(),resp.data(),resp.size());
    auto datanode=DataNode(node,resp.size());
    Send(datanode);

    Play();
}

void Rtsp_Connection::TeardownHandler(std::map<std::string,std::string>& maps) {
    auto resp=fmt::format("RTSP/1.0 200 OK\r\n"
                          "CSeq: {}\r\n"
                          "Server: {}\r\n"
                          "\r\n",maps["CSeq"],SERVER_VERSION);
    std::shared_ptr<char> node=std::shared_ptr<char>(new char[resp.size()]);
    memcpy(node.get(),resp.data(),resp.size());
    auto datanode=DataNode(node,resp.size());
    Send(datanode);

    Teardown();
}

std::string Rtsp_Connection::generateSDPDescription() {
    if(StreamLinkMap.empty()){
        return {};
    }
    auto resp=fmt::format("v=0\r\n"
                          "o=- 9{} 1 IN IP4 {}\r\n"
                          "t=0 0\r\n"
                          "a=control:*\r\n"
                          "a=type:broadcast\r\n",(long) time(nullptr),"0.0.0.0");
    for(auto&&[key,val]:StreamLinkMap){
        resp+=fmt::format("{}\r\n",val->GetGop()->GetSink()->GetMediaDescription(UdpListenPort));

        //TODO 实现多播选项，现暂无
        resp+=fmt::format("c=IN IP4 {}\r\n","0.0.0.0");

        resp+=fmt::format("{}\r\n",val->GetGop()->GetSink()->GetAttribute());
        resp+=fmt::format("a=control:track{}\r\n",key);
    }
    return resp;


}

void Rtsp_Connection::Teardown() {
    for(auto&&[key,val]:StreamLinkMap){
        val->StopTimer();
        val->GetGop()->GetSink()->StopTimer();
    }
    StreamLinkMap.clear();
    if(!std::dynamic_pointer_cast<Rtsp_Server>(Server)->SessionMap.contains(LivePath))
        return;
    std::dynamic_pointer_cast<Rtsp_Server>(Server)->SessionMap[LivePath]->RemoveConn(GetID());
}

