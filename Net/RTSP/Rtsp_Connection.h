//
// Created by luyoi on 2023/10/16.
//

#ifndef MEDIASIMPLESERVER_RTSP_CONNECTION_H
#define MEDIASIMPLESERVER_RTSP_CONNECTION_H

#include<boost/asio.hpp>
#include<random>
#include<queue>
#include<mutex>
#include<condition_variable>
#include<thread>
#include<map>
#include<functional>
#include<spdlog/spdlog.h>
#include"Rtsp_Udp_Server.h"
#include"../../Tool/Session.h"
#include"../../Tool/StreamLinkState.h"
#include"Rtsp_Tool.h"
#include"../Tcp_Server.h"




class Rtsp_Connection: public Tcp_Connection{

public:
    Rtsp_Connection(boost::asio::io_context& ioc_,std::shared_ptr<Tcp_Server> Main);
    ~Rtsp_Connection();


    void SendRtpPacket(std::shared_ptr<RtpPacket> node, unsigned short Channel, unsigned short port,std::shared_ptr<Tcp_Connection> thisNode);
private:
    void Init();
    void SendUdpPacket(std::shared_ptr<UdpNode>& node,unsigned short port);
    void SendTcpPacket(DataNode& node);
    virtual size_t Process(DataNode& buf);

    //交互处理函数
    void OptionHandler(std::map<std::string,std::string>& maps);
    void DescribeHandler(std::map<std::string,std::string>& maps);
    void SetupHandler(std::map<std::string,std::string>& maps);
    void PlayHandler(std::map<std::string,std::string>& maps);
    void TeardownHandler(std::map<std::string,std::string>& maps);
    void Play();//play 指令后续处理
    void Teardown();
    std::string  generateSDPDescription();



    ConnType type;
    std::string LivePath;
    std::map<RtspMethod,std::function<void(std::map<std::string,std::string>&)>> MethodFunc;

    //TODO 实现Rtcp数据包定时发送，报告连接情况

    std::map<uint16_t,std::shared_ptr<StreamLinkState>> StreamLinkMap;

};


#endif //MEDIASIMPLESERVER_RTSP_CONNECTION_H
