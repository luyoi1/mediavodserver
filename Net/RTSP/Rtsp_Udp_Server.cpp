//
// Created by luyoi on 2023/10/21.
//

#include "Rtsp_Udp_Server.h"

#include <utility>

Rtsp_Udp_Server::Rtsp_Udp_Server(unsigned short RtpPort,std::queue<std::shared_ptr<UdpNode>>& Output,std::mutex& OutMutex):
        RtpSock(IOContextPool::GetInstance()->GetContext(),boost::asio::ip::udp::endpoint(boost::asio::ip::make_address("0.0.0.0"),RtpPort)),
        RtcpSock(IOContextPool::GetInstance()->GetContext(),boost::asio::ip::udp::endpoint(boost::asio::ip::make_address("0.0.0.0"),RtpPort+1)),
        OutputQueue(Output),OutputMutex(OutMutex),
        RtpBuffer(new unsigned char[Recv_Udp_Buffer_Len]),
        RtcpBuffer(new unsigned char[Recv_Udp_Buffer_Len])
{

}

Rtsp_Udp_Server::~Rtsp_Udp_Server() {
    if(RtpSock.is_open()){
        RtpSock.cancel();
        RtpSock.close();
    }
    if(RtcpSock.is_open()){
        RtcpSock.cancel();
        RtcpSock.close();
    }
}

void Rtsp_Udp_Server::PushNode(std::shared_ptr<UdpNode> node) {
    RtpSock.send_to(boost::asio::buffer(node->data.get(),node->dataLength),
                    boost::asio::ip::udp::endpoint(boost::asio::ip::make_address(node->IP),node->Port));
    return;
}

void Rtsp_Udp_Server::RecvRtpHandler(boost::system::error_code ec, size_t byteSize) {
    if(ec){
        spdlog::error("RecvRtp Func Error,Error is {}",ec.what());
        return;
    }
    //组包
    auto node=std::make_shared<UdpNode>(SendType::RTP,"0.0.0.0",0,byteSize);
    memcpy(node->data.get(),RtpBuffer.get(),node->dataLength);
    std::unique_lock<std::mutex> uql(OutputMutex);
    PushOutputQue(node);
    memset(RtpBuffer.get(),0,Recv_Udp_Buffer_Len);
    RtpSock.async_receive(boost::asio::buffer(RtpBuffer.get(),Recv_Udp_Buffer_Len)
                          ,std::bind(&Rtsp_Udp_Server::RecvRtpHandler,this,std::placeholders::_1,std::placeholders::_2));
}

void Rtsp_Udp_Server::RecvRtcpHandler(boost::system::error_code ec, size_t byteSize) {
    if(ec){
        spdlog::error("RecvRtcp Func Error,Error is {}",ec.what());
        return;
    }
    //组包
    auto node=std::make_shared<UdpNode>(SendType::RTCP,"0.0.0.0",0,byteSize);
    memcpy(node->data.get(),RtcpBuffer.get(),node->dataLength);
    std::unique_lock<std::mutex> uql(OutputMutex);
    PushOutputQue(node);
    memset(RtcpBuffer.get(),0,Recv_Udp_Buffer_Len);
    RtcpSock.async_receive(boost::asio::buffer(RtcpBuffer.get(),Recv_Udp_Buffer_Len)
            ,std::bind(&Rtsp_Udp_Server::RecvRtpHandler,this,std::placeholders::_1,std::placeholders::_2));
}

void Rtsp_Udp_Server::SendHandler(boost::system::error_code ec,SendType type) {
    if(ec){
        spdlog::error("RtspUdpServer Send Error!Error is {}",ec.what());
        return;
    }
    if(type==SendType::RTP){
        std::unique_lock uql(SendRtpMutex);
        SendRtpQueue.pop();
        auto FrontNode=SendRtpQueue.front();
        RtpSock.async_send_to(boost::asio::buffer(FrontNode->data.get(),FrontNode->dataLength),
                              boost::asio::ip::udp::endpoint(boost::asio::ip::make_address(FrontNode->IP),FrontNode->Port),
                              std::bind(&Rtsp_Udp_Server::SendHandler,this,std::placeholders::_1,SendType::RTP));
    }else if(type==SendType::RTCP){
        std::unique_lock uql(SendRtcpMutex);
        SendRtcpQueue.pop();
        auto FrontNode=SendRtcpQueue.front();
        RtcpSock.async_send_to(boost::asio::buffer(FrontNode->data.get(),FrontNode->dataLength),
                               boost::asio::ip::udp::endpoint(boost::asio::ip::make_address(FrontNode->IP),FrontNode->Port),
                               std::bind(&Rtsp_Udp_Server::SendHandler,this,std::placeholders::_1,SendType::RTP));
    }


}

void Rtsp_Udp_Server::Start() {
    memset(RtpBuffer.get(),0,Recv_Udp_Buffer_Len);
    memset(RtcpBuffer.get(),0,Recv_Udp_Buffer_Len);
    RtpSock.async_receive(boost::asio::buffer(RtpBuffer.get(),Recv_Udp_Buffer_Len)
            ,std::bind(&Rtsp_Udp_Server::RecvRtpHandler,this,std::placeholders::_1,std::placeholders::_2));
    RtcpSock.async_receive(boost::asio::buffer(RtcpBuffer.get(),Recv_Udp_Buffer_Len)
            ,std::bind(&Rtsp_Udp_Server::RecvRtpHandler,this,std::placeholders::_1,std::placeholders::_2));
}

void Rtsp_Udp_Server::PushRtpNode(std::shared_ptr<UdpNode> node) {
    std::unique_lock<std::mutex> uql(SendRtpMutex);
    SendRtpQueue.push(node);
    if(SendRtpQueue.size()>1){
        return;
    }
    auto FrontNode=SendRtpQueue.front();
    RtpSock.async_send_to(boost::asio::buffer(FrontNode->data.get(),FrontNode->dataLength),
                          boost::asio::ip::udp::endpoint(boost::asio::ip::make_address(FrontNode->IP),FrontNode->Port),
                          std::bind(&Rtsp_Udp_Server::SendHandler,this,std::placeholders::_1,SendType::RTP));
}

void Rtsp_Udp_Server::PushRtcpNode(std::shared_ptr<UdpNode> node) {
    std::unique_lock<std::mutex> uql(SendRtpMutex);
    SendRtpQueue.push(node);
    if(SendRtpQueue.size()>1){
        return;
    }
    auto FrontNode=SendRtcpQueue.front();
    RtcpSock.async_send_to(boost::asio::buffer(FrontNode->data.get(),FrontNode->dataLength),
                          boost::asio::ip::udp::endpoint(boost::asio::ip::make_address(FrontNode->IP),FrontNode->Port),
                          std::bind(&Rtsp_Udp_Server::SendHandler,this,std::placeholders::_1,SendType::RTP));
}

void Rtsp_Udp_Server::PushOutputQue(std::shared_ptr<UdpNode> node) {
    std::unique_lock<std::mutex> uql(OutputMutex);
    OutputQueue.push(node);
}

void Rtsp_Udp_Server::SendTo(const char *data, size_t dataSize, unsigned int port, std::string_view ip) {
    RtpSock.send_to(boost::asio::buffer(data,dataSize),
                    boost::asio::ip::udp::endpoint(boost::asio::ip::make_address(ip.data()),port));
}

UdpNode::UdpNode(SendType type, std::string ip, unsigned short port, unsigned int dataSize):
    Type(type),IP(std::move(ip)),Port(port),dataLength(dataSize),data(new unsigned char[dataLength])
{

}
