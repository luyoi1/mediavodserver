//
// Created by luyoi on 2023/10/16.
//

#ifndef MEDIASIMPLESERVER_RTSP_SERVER_H
#define MEDIASIMPLESERVER_RTSP_SERVER_H

#include<boost/asio.hpp>
#include<map>
#include<spdlog/spdlog.h>
#include<memory>
#include "Rtsp_Connection.h"
#include "../../Tool/Session.h"
#include"../Tcp_Server.h"

class Rtsp_Server: public Tcp_Server {
    friend class Rtsp_Connection;
public:
    Rtsp_Server(std::string ip,unsigned short RtspPort,unsigned short RtpUdpPort);
    ~Rtsp_Server();

    virtual void Start();
    void AddSession(std::string_view Name,const std::shared_ptr<Session>& session);

private:
    virtual void HandleAccept(boost::system::error_code ec,std::shared_ptr<Tcp_Connection> conn);

    std::queue<std::shared_ptr<UdpNode>> RecvUdpQue;
    std::mutex  RecvMutex;
    std::shared_ptr<Rtsp_Udp_Server> UdpServer;
    std::mutex SessionMtx;
    std::map<std::string,std::shared_ptr<Session>> SessionMap;//存储会话
};


#endif //MEDIASIMPLESERVER_RTSP_SERVER_H
