//
// Created by luyoi on 2023/10/20.
//

#ifndef MEDIASIMPLESERVER_RTSP_TOOL_H
#define MEDIASIMPLESERVER_RTSP_TOOL_H

#endif //MEDIASIMPLESERVER_RTSP_TOOL_H

#include<vector>
#include<string>
#include<map>

#pragma once

static std::vector<std::string> split(const std::string& str,std::string pattern,int maxSize){
    std::vector<std::string> result{};
    std::string::size_type index=0,pre=0;
    for(;index<maxSize;){
        index=str.find(pattern,pre);
        if(index<=maxSize){
            result.push_back(str.substr(pre,index-pre));
            pre=index+pattern.size();
            continue;
        }
        result.push_back(str.substr(pre));
    }
    return result;
}

static std::map<std::string,RtspMethod> MethodNames{
        {"OPTIONS",RtspMethod::OPTIONS},
        {"DESCRIBE",RtspMethod::DESCRIBE},
        {"SETUP",RtspMethod::SETUP},
        {"PLAY",RtspMethod::PLAY},
        {"TEARDOWN",RtspMethod::TEARDOWN},
        {"NONE",RtspMethod::NONE}
};

static RtspMethod ParseRtspSeq(std::string& seq,std::map<std::string,std::string>& maps){
    auto lineStrs= split(seq,std::string("\r\n"),seq.size()-4);
    RtspMethod method;
    for(int i=0;i<lineStrs.size();i++){
        if(i==0){
            auto result=split(lineStrs[i]," ",lineStrs[i].size());
            maps["Method"]=result[0];
            maps["Url"]=result[1];
            maps["Version"]=result[2];
            method=MethodNames[maps["Method"]];
            continue;
        }
        auto a=lineStrs[i].find(": ");
        auto key=lineStrs[i].substr(0,a);
        auto val=lineStrs[i].substr(a+2);
        maps.emplace(key,val);
    }
    return method;
}